/** GtkTerminalExample .java
 */ 
package vte;

import org.gnu.gnomevte.Terminal;
import org.gnu.gtk.AttachOptions;
import org.gnu.gtk.Gtk;
import org.gnu.gtk.GtkStockItem;
import org.gnu.gtk.Notebook;
import org.gnu.gtk.Table;
import org.gnu.gtk.ToolBar;
import org.gnu.gtk.ToolButton;
import org.gnu.gtk.Widget;
import org.gnu.gtk.Window;
import org.gnu.gtk.WindowType;
import org.gnu.gtk.event.LifeCycleEvent;
import org.gnu.gtk.event.LifeCycleListener;
import org.gnu.gtk.event.NotebookEvent;
import org.gnu.gtk.event.NotebookListener;
import org.gnu.gtk.event.ToolButtonEvent;
import org.gnu.gtk.event.ToolButtonListener;

public class GtkTerminalExample extends Window {
	private ToolBar toolbar;
	private Terminal term;
	private Table table;
	private Notebook notebook;
	private int pageCount=0;
	
	public GtkTerminalExample (String appTitle){
		super(WindowType.TOPLEVEL);
		super.setTitle(appTitle);
		this.addListener(new LifeCycleListener() {
			public void lifeCycleEvent(LifeCycleEvent event) {}
			public boolean lifeCycleQuery(LifeCycleEvent event) {
				if (event.isOfType(LifeCycleEvent.Type.DESTROY) || 
						event.isOfType(LifeCycleEvent.Type.DELETE)) {
						Gtk.mainQuit();
				}
				return false;
			}
		});
		//resize(800,600);
		table = new Table(2, 1, false);
		notebook = new Notebook();
		initGUI();
			
	}

	private void initGUI(){
		notebook.addListener(new NotebookListener(){
			public void notebookEvent(NotebookEvent evt){
				handleNotebookEvent(evt);
			}
		});
			
		toolbar = createMyToolbar();
		//toolbar.setResizeMode(ResizeMode.PARENT);
		
		table.attach(toolbar, 0, 1, 0, 1,AttachOptions.FILL, AttachOptions.SHRINK, 0, 0);
		table.attach(notebook, 0, 1, 1, 2,AttachOptions.FILL, AttachOptions.FILL, 0, 0);
		
		term = Terminal.terminalAndShell();
		notebook.appendPage(term, null);
		pageCount++;
		
		add(table);
		showAll();
	}
		


	public ToolBar createMyToolbar (){
		toolbar = new ToolBar();
		
		ToolButton bquit = new ToolButton(GtkStockItem.QUIT);
		bquit.addListener(new ToolButtonListener() {
			public boolean toolButtonEvent(ToolButtonEvent evt) {
				Gtk.mainQuit();
				return true;
			}
		});
		toolbar.insert(bquit, 0);
		
		ToolButton bnew = new ToolButton(GtkStockItem.NEW);
		bnew.addListener(new ToolButtonListener(){
			public boolean toolButtonEvent(ToolButtonEvent evt){
				handleButtonEvent(evt);
				return true;
			}
		});
		toolbar.insert(bnew, 1);
		
		ToolButton bclose = new ToolButton(GtkStockItem.CLOSE);
		bclose.addListener(new ToolButtonListener(){
			public boolean toolButtonEvent(ToolButtonEvent evt){
				closePressed(evt);
				return true;
			}
		});
		toolbar.insert(bclose, 2);
		
		ToolButton bgo_back = new ToolButton(GtkStockItem.GO_BACK);
		bgo_back.addListener(new ToolButtonListener(){
			public boolean toolButtonEvent(ToolButtonEvent evt){
				handlePrev(evt);
				return true;
			}
		});
		toolbar.insert(bgo_back,3);
		
		ToolButton bgo_forward = new ToolButton(GtkStockItem.GO_FORWARD);
		bgo_forward.addListener(new ToolButtonListener(){
			public boolean toolButtonEvent (ToolButtonEvent evt){
				handleNext(evt);
				return true;
			}
		});
		toolbar.insert(bgo_forward, 4);
		
		return toolbar;
	}

	public static void main(String args[]){
		Gtk.init(args);
		new GtkTerminalExample ("Terminal Example"); 
		Gtk.main();
	}

	public void handleButtonEvent(ToolButtonEvent evt){
		Terminal t = Terminal.terminalAndShell();
		t.show();
		notebook.appendPage(t, null);
		pageCount++;
		int num = notebook.pageNum(t);
		notebook.setCurrentPage(num);
		t.grabFocus();
	}

	public void handleNotebookEvent(NotebookEvent evt){
		
	}


	public void closePressed(ToolButtonEvent evt){
		if(pageCount==1)
			return;
		int current = notebook.getCurrentPage();
		System.out.println("Page " + current + "colsed.");
		notebook.removePage(current);
		pageCount--;
		current = notebook.getCurrentPage();
		Widget w = notebook.getPage(current);
		w.grabFocus();
	}

	protected void handleNext(ToolButtonEvent evt){
		notebook.nextPage();
	}

	protected void handlePrev(ToolButtonEvent evt){
		notebook.prevPage();
	}		
		
}
//GtkTerminalExample .java 

