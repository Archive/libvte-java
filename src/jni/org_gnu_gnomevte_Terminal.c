/*
 *  
 *  Java-Gnome Bindings Library
 *
 *  Copyright 1998-2002 the Java-Gnome Team, all rights reserved.
 *  
 *  This is free software; you can redistribute it and/or modify it under
 *  the terms of the GNU Library General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *  
 *  This program is distributed in the hope that it will be useful, but
 *  WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 *  General Public License for more details.
 *  
 *  You should have received a copy of the GNU Library General Public
 *  License along with this program; if not, write to the Free Software
 *  Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 */



#include <jni.h>
#include <vte/vte.h>
#include <unistd.h>
#include <glib-java/jg_jnu.h>
#include <gtk_java.h>
#include "org_gnu_gnomevte_Terminal.h"

extern char **environ;

#ifndef _Included_org_gnu_gnomevte_Terminal
#define _Included_org_gnu_gnomevte_Terminal
#ifdef __cplusplus
extern "C" {
#endif

/*
 * Class:     org_gnu_gnomevte_Terminal
 * Method:    vte_terminal_new
 */
JNIEXPORT jobject JNICALL Java_org_gnu_gnomevte_Terminal_vte_1terminal_1new
  (JNIEnv *env, jclass klass)
{

	return getGObjectHandle(env, G_OBJECT(vte_terminal_new()));
}


/*
 * Class:     org_gnu_gnomevte_Terminal
 * Method:    vte_terminal_fork_command
 */
JNIEXPORT jint JNICALL Java_org_gnu_gnomevte_Terminal_vte_1terminal_1fork_1command
  (JNIEnv *env, jclass klass, jobject handle, jstring command, jobjectArray argv, 
   jstring directory, jboolean lastlog, jboolean utmp, jboolean wtmp)
{
	VteTerminal* term = (VteTerminal*)getPointerFromHandle(env, handle);
	gchar **args_g = getStringArray(env, argv);
	gchar *command_g = command ? (gchar*)(*env)->GetStringUTFChars(env, command, NULL) : NULL;
	gchar *directory_g = directory ? (gchar*)(*env)->GetStringUTFChars(env, directory, NULL) : NULL;
	int retval = vte_terminal_fork_command (term, command_g, args_g, environ, directory_g, 
			(gboolean)lastlog, (gboolean)utmp, (gboolean)wtmp);
	freeStringArray(env, argv, args_g);
	if (command)
		(*env)->ReleaseStringUTFChars(env, command, command_g);
	if (directory)
		(*env)->ReleaseStringUTFChars(env, directory, directory_g);
	return retval;
 }

 /*
  * Class:     org_gnu_gnomevte_Terminal
 * Method:    vte_terminal_set_pty
 */
JNIEXPORT void JNICALL Java_org_gnu_gnomevte_Terminal_vte_1terminal_1set_1pty
  (JNIEnv *env, jclass klass, jobject handle, jint pty_master)
{
	VteTerminal* terminal = (VteTerminal*)getPointerFromHandle(env, handle);
	vte_terminal_set_pty (terminal, (gint)pty_master);
}

/*
 * Class:     org_gnu_gnomevte_Terminal
 * Method:    vte_terminal_feed
 */
JNIEXPORT void JNICALL Java_org_gnu_gnomevte_Terminal_vte_1terminal_1feed
  (JNIEnv *env, jclass klass, jobject handle, jstring data, jint length)
{
	VteTerminal* terminal = (VteTerminal*)getPointerFromHandle(env, handle);
	gchar* data_g = (gchar*)(*env)->GetStringUTFChars(env, data, NULL);
	vte_terminal_feed (terminal, data_g, (gint)length);
	(*env)->ReleaseStringUTFChars(env, data, data_g);	
}

/*
 * Class:     org_gnu_gnomevte_Terminal
 * Method:    vte_terminal_feed_child
 */
JNIEXPORT void JNICALL Java_org_gnu_gnomevte_Terminal_vte_1terminal_1feed_1child
  (JNIEnv *env, jclass klass, jobject handle, jstring data, jlong length)
{
	VteTerminal* terminal = (VteTerminal*)getPointerFromHandle(env, handle);
	gchar* data_g = (gchar*)(*env)->GetStringUTFChars(env, data, NULL);
	vte_terminal_feed_child (terminal, data_g, (gint)length);
	(*env)->ReleaseStringUTFChars(env, data, data_g);	
}

/*
 * Class:     org_gnu_gnomevte_Terminal
 * Method:    vte_terminal_copy_clipboard
 */
JNIEXPORT void JNICALL Java_org_gnu_gnomevte_Terminal_vte_1terminal_1copy_1clipboard
  (JNIEnv *env, jclass klass, jobject handle)
{
	VteTerminal* terminal = (VteTerminal*)getPointerFromHandle(env, handle);
	vte_terminal_copy_clipboard (terminal);
}
	  
/*
 * Class:     org_gnu_gnomevte_Terminal
 * Method:    vte_terminal_paste_clipboard
 */
JNIEXPORT void JNICALL Java_org_gnu_gnomevte_Terminal_vte_1terminal_1paste_1clipboard
  (JNIEnv *env, jclass klass, jobject handle)
{
	VteTerminal* terminal = (VteTerminal*)getPointerFromHandle(env, handle);
	vte_terminal_paste_clipboard (terminal);
}

/*
 * Class:     org_gnu_gnomevte_Terminal
 * Method:    vte_terminal_copy_primary
 */
JNIEXPORT void JNICALL Java_org_gnu_gnomevte_Terminal_vte_1terminal_1copy_1primary
  (JNIEnv *env, jclass klass, jobject handle) 
{
	VteTerminal* terminal = (VteTerminal*)getPointerFromHandle(env, handle);
	vte_terminal_copy_primary(terminal);
}

/*
 * Class:     org_gnu_gnomevte_Terminal
 * Method:    vte_terminal_paste_primary
 */
JNIEXPORT void JNICALL Java_org_gnu_gnomevte_Terminal_vte_1terminal_1paste_1primary
  (JNIEnv *env, jclass klass, jobject handle) 
{
	VteTerminal* terminal = (VteTerminal*)getPointerFromHandle(env, handle);
	vte_terminal_paste_primary(terminal);
}

/*
 * Class:     org_gnu_gnomevte_Terminal
 * Method:    vte_terminal_set_size
 */
JNIEXPORT void JNICALL Java_org_gnu_gnomevte_Terminal_vte_1terminal_1set_1size
  (JNIEnv *env, jclass klass, jobject handle, jint columns, jint rows)
{
	VteTerminal* terminal = (VteTerminal*)getPointerFromHandle(env, handle);
	vte_terminal_set_size (terminal, (gint)columns, (gint)rows);
}

/*
 * Class:     org_gnu_gnomevte_Terminal
 * Method:    vte_terminal_set_audible_bell
 */
JNIEXPORT void JNICALL Java_org_gnu_gnomevte_Terminal_vte_1terminal_1set_1audible_1bell
  (JNIEnv *env, jclass klass, jobject handle, jboolean audible)
{
	VteTerminal* terminal = (VteTerminal*)getPointerFromHandle(env, handle);
	vte_terminal_set_audible_bell (terminal, (gboolean)audible);
}
	  
/*
 * Class:     org_gnu_gnomevte_Terminal
 * Method:    vte_terminal_get_audible_bell
 */
JNIEXPORT jboolean JNICALL Java_org_gnu_gnomevte_Terminal_vte_1terminal_1get_1audible_1bell
  (JNIEnv *env, jclass klass, jobject handle)
{
	VteTerminal* terminal = (VteTerminal*)getPointerFromHandle(env, handle);
	return (gboolean)vte_terminal_get_audible_bell (terminal);
}
	  
/*
 * Class:     org_gnu_gnomevte_Terminal
 * Method:    vte_terminal_set_visible_bell
 */
JNIEXPORT void JNICALL Java_org_gnu_gnomevte_Terminal_vte_1terminal_1set_1visible_1bell
  (JNIEnv *env, jclass klass, jobject handle, jboolean visible)
{
	VteTerminal* term = (VteTerminal*)getPointerFromHandle(env, handle);
	vte_terminal_set_visible_bell (term, (gboolean)visible);
}

/*
 * Class:     org_gnu_gnomevte_Terminal
 * Method:    vte_terminal_get_visible_bell
 */
JNIEXPORT jboolean JNICALL Java_org_gnu_gnomevte_Terminal_vte_1terminal_1get_1visible_1bell
  (JNIEnv *env, jclass klass, jobject handle)
{
	VteTerminal* terminal = (VteTerminal*)getPointerFromHandle(env, handle);
	return vte_terminal_get_visible_bell (terminal);
}

/*
 * Class:     org_gnu_gnomevte_Terminal
 * Method:    vte_terminal_set_scroll_on_output
 */
JNIEXPORT void JNICALL Java_org_gnu_gnomevte_Terminal_vte_1terminal_1set_1scroll_1on_1output
  (JNIEnv *env, jclass klass, jobject handle, jboolean scroll)
{

	VteTerminal *term = (VteTerminal*)getPointerFromHandle(env, handle);
	vte_terminal_set_scroll_on_output (term, (gboolean)scroll);
}

/*
 * Class:     org_gnu_gnomevte_Terminal
 * Method:    vte_terminal_set_scroll_on_keystroke
 */
JNIEXPORT void JNICALL Java_org_gnu_gnomevte_Terminal_vte_1terminal_1set_1scroll_1on_1keystroke
  (JNIEnv *env, jclass klass, jobject handle, jboolean scroll)
{
	VteTerminal *term = (VteTerminal*)getPointerFromHandle(env, handle);
	vte_terminal_set_scroll_on_keystroke (term, (gboolean)scroll);
}

/*
 * Class:     org_gnu_gnomevte_Terminal
 * Method:    vte_terminal_set_color_dim
 */
JNIEXPORT void JNICALL Java_org_gnu_gnomevte_Terminal_vte_1terminal_1set_1color_1dim
	(JNIEnv *env, jclass klass, jobject handle, jobject dim) 
{
	VteTerminal *term = (VteTerminal*)getPointerFromHandle(env, handle);
	GdkColor *g_dim = (GdkColor*)getPointerFromHandle(env, dim);
	vte_terminal_set_color_dim(term, g_dim);
}
	
/*
 * Class:     org_gnu_gnomevte_Terminal
 * Method:    vte_terminal_set_color_bold
 */
JNIEXPORT void JNICALL Java_org_gnu_gnomevte_Terminal_vte_1terminal_1set_1color_1bold
  (JNIEnv *env, jclass klass, jobject handle, jobject color) 
{
	VteTerminal *term = (VteTerminal*)getPointerFromHandle(env, handle);
	GdkColor *g_color = (GdkColor*)getPointerFromHandle(env, color);
	vte_terminal_set_color_bold(term, g_color);
}  

/*
 * Class:     org_gnu_gnomevte_Terminal
 * Method:    vte_terminal_set_color_foreground
 */
JNIEXPORT void JNICALL Java_org_gnu_gnomevte_Terminal_vte_1terminal_1set_1color_1foreground
  (JNIEnv *env, jclass klass, jobject handle, jobject fgcolor) 
{
	VteTerminal *term = (VteTerminal*)getPointerFromHandle(env, handle);
	GdkColor *g_fgcolor = (GdkColor*)getPointerFromHandle(env, fgcolor);
	vte_terminal_set_color_foreground(term, g_fgcolor);
}

/*
 * Class:     org_gnu_gnomevte_Terminal
 * Method:    vte_terminal_set_color_background
 */
JNIEXPORT void JNICALL Java_org_gnu_gnomevte_Terminal_vte_1terminal_1set_1color_1background
  (JNIEnv *env, jclass klass, jobject handle, jobject bgcolor) 
{
	VteTerminal *term = (VteTerminal*)getPointerFromHandle(env, handle);
	GdkColor *g_bgcolor = (GdkColor*)getPointerFromHandle(env, bgcolor);
	vte_terminal_set_color_background(term, g_bgcolor);
}

/*
 * Class:     org_gnu_gnomevte_Terminal
 * Method:    vte_terminal_set_colors
 */
JNIEXPORT void JNICALL Java_org_gnu_gnomevte_Terminal_vte_1terminal_1set_1colors
  (JNIEnv *env, jclass klass, jobject handle, jobject foreground, jobject background, 
  		jobject palett, jint palett_size) 
{
	VteTerminal *term = (VteTerminal*)getPointerFromHandle(env, handle);
	GdkColor *g_fgcolor = (GdkColor*)getPointerFromHandle(env, foreground);
	GdkColor *g_bgcolor = (GdkColor*)getPointerFromHandle(env, background);
	GdkColor *g_palett = (GdkColor*)getPointerFromHandle(env, palett);
	vte_terminal_set_colors(term, g_fgcolor, g_bgcolor, g_palett, (glong)palett_size);
}		

/*
 * Class:     org_gnu_gnomevte_Terminal
 * Method:    vte_terminal_set_default_colors
 */
JNIEXPORT void JNICALL Java_org_gnu_gnomevte_Terminal_vte_1terminal_1set_1default_1colors
  (JNIEnv *env, jclass klass, jobject handle)
{
	VteTerminal *term = (VteTerminal*)getPointerFromHandle(env, handle);
	vte_terminal_set_default_colors (term);
}

/*
 * Class:     org_gnu_gnomevte_Terminal
 * Method:    vte_terminal_set_background_image
 */
JNIEXPORT void JNICALL Java_org_gnu_gnomevte_Terminal_vte_1terminal_1set_1background_1image
  (JNIEnv *env, jclass klass, jobject handle, jobject pixbuf)
{
	GdkPixbuf *pix = (GdkPixbuf*)getPointerFromHandle(env, pixbuf);
	VteTerminal *term = (VteTerminal*)getPointerFromHandle(env, handle);
	vte_terminal_set_background_image (term, pix);
}

/*
 * Class:     org_gnu_gnomevte_Terminal
 * Method:    vte_terminal_set_background_image_file
 */
JNIEXPORT void JNICALL Java_org_gnu_gnomevte_Terminal_vte_1terminal_1set_1background_1image_1file
  (JNIEnv *env, jclass klass, jobject handle, jstring file)
{
	gchar *file_g = (gchar*)(*env)->GetStringUTFChars(env, file, NULL);
	VteTerminal *term = (VteTerminal*)getPointerFromHandle(env, handle);
	vte_terminal_set_background_image_file(term, file_g);
	(*env)->ReleaseStringUTFChars(env, file, file_g);
}

/*
 * Class:     org_gnu_gnomevte_Terminal
 * Method:    vte_terminal_set_background_saturation
 */
JNIEXPORT void JNICALL Java_org_gnu_gnomevte_Terminal_vte_1terminal_1set_1background_1saturation
  (JNIEnv *env, jclass klass, jobject handle, jdouble saturation)
{
	VteTerminal *term = (VteTerminal*)getPointerFromHandle(env, handle);
	vte_terminal_set_background_saturation (term, (double)saturation);
}

/*
 * Class:     org_gnu_gnomevte_Terminal
 * Method:    vte_terminal_set_background_transparent
 */
JNIEXPORT void JNICALL Java_org_gnu_gnomevte_Terminal_vte_1terminal_1set_1background_1transparent
  (JNIEnv *env, jclass klass, jobject handle, jboolean transparent)
{
	VteTerminal *term = (VteTerminal*)getPointerFromHandle(env, handle);
	vte_terminal_set_background_transparent (term, (gboolean)transparent);
}

/*
 * Class:     org_gnu_gnomevte_Terminal
 * Method:    vte_terminal_set_cursor_blinks
 */
JNIEXPORT void JNICALL Java_org_gnu_gnomevte_Terminal_vte_1terminal_1set_1cursor_1blinks
  (JNIEnv *env, jclass klass, jobject handle, jboolean blinking)
{
	VteTerminal *term = (VteTerminal*)getPointerFromHandle(env, handle);
	vte_terminal_set_cursor_blinks (term, (gboolean)blinking);
}


/*
 * Class:     org_gnu_gnomevte_Terminal
 * Method:    vte_terminal_set_scrollback_lines
 */
JNIEXPORT void JNICALL Java_org_gnu_gnomevte_Terminal_vte_1terminal_1set_1scrollback_1lines
  (JNIEnv *env, jclass klass, jobject handle, jint lines)
{
	VteTerminal *term = (VteTerminal*)getPointerFromHandle(env, handle);
	vte_terminal_set_scrollback_lines (term, lines);
}

#if 0
/* XXX: cagney 2006-02-28: The corresponding method in the .java file
   was commented out.  */
/*
 * Class:     org_gnu_gnomevte_Terminal
 * Method:    vte_terminal_im_append_menuitems
 */
JNIEXPORT void JNICALL Java_org_gnu_gnomevte_Terminal_vte_1terminal_1im_1append_1menuitems
  (JNIEnv *env, jclass klass, jobject handle, jobject menuShell)
{
	VteTerminal *term = (VteTerminal*)getPointerFromHandle(env, handle);
	GtkMenuShell *menu = (GtkMenuShell*)getPointerFromHandle(env, menuShell);
	vte_terminal_im_append_menuitems(term, menu);
}
#endif
	  
/*
 * Class:     org_gnu_gnomevte_Terminal
 * Method:    vte_terminal_set_font
 */
//TODO:
JNIEXPORT void JNICALL Java_org_gnu_gnomevte_Terminal_vte_1terminal_1set_1font
  (JNIEnv *env, jclass klass, jobject handle, jobject font) 
{
	VteTerminal *term = (VteTerminal*)getPointerFromHandle(env, handle);
	PangoFontDescription *p_font = (PangoFontDescription*)getPointerFromHandle(env, font);
	vte_terminal_set_font(term, p_font);
}  

#if 0
/* XXX: cagney 2006-02-28: The corresponding method in the .java file
   was commented out.  */
/*
 * Class:     org_gnu_gnomevte_Terminal
 * Method:    vte_terminal_set_font_from_string
 */
JNIEXPORT void JNICALL Java_org_gnu_gnomevte_Terminal_vte_1terminal_1set_1font_1from_1string
  (JNIEnv *env, jclass klass, jobject handle, jbyteArray font)
{
	VteTerminal *term = (VteTerminal*)getPointerFromHandle(env, handle);
	jbyte *font_g_byte = font ? (*env)->GetByteArrayElements(env, font, 0) : NULL;
	gchar *font_g = (gchar*)font_g_byte;
	vte_terminal_set_font_from_string(term, font_g);
	(*env)->ReleaseByteArrayElements(env, font, font_g_byte, 0);
}
#endif
	  
/*
 * Class:     org_gnu_gnomevte_Terminal
 * Method:    vte_terminal_get_font
 */
JNIEXPORT jobject JNICALL Java_org_gnu_gnomevte_Terminal_vte_1terminal_1get_1font
  (JNIEnv *env, jclass klass, jobject handle)
{
	VteTerminal *term = (VteTerminal*)getPointerFromHandle(env, handle);
	return getGBoxedHandle(env, 
				(PangoFontDescription*)vte_terminal_get_font(term),
				PANGO_TYPE_FONT_DESCRIPTION,
				(GBoxedCopyFunc)pango_font_description_copy,
				(GBoxedFreeFunc)pango_font_description_free);

}

#if 0
/* XXX: cagney 2006-02-28: The corresponding method in the .java file
   was commented out.  */
/*
 * Class:     org_gnu_gnomevte_Terminal
 * Method:    vte_terminal_get_using_xft
 */
JNIEXPORT jboolean JNICALL Java_org_gnu_gnomevte_Terminal_vte_1terminal_1get_1using_1xft
  (JNIEnv *env, jclass klass, jobject handle)
{
	VteTerminal *term = (VteTerminal*)getPointerFromHandle(env, handle);
	return (jboolean) vte_terminal_get_using_xft(term);
}
#endif

/*
 * Class:     org_gnu_gnomevte_Terminal
 * Method:    vte_terminal_set_allow_bold
 */
JNIEXPORT void JNICALL Java_org_gnu_gnomevte_Terminal_vte_1terminal_1set_1allow_1bold
  (JNIEnv *env, jclass klass, jobject handle, jboolean allow)
{
	gboolean _allow = (gboolean)allow;
	VteTerminal *term = (VteTerminal*)getPointerFromHandle(env, handle);
	vte_terminal_set_allow_bold(term, _allow);
}

/*
 * Class:     org_gnu_gnomevte_Terminal
 * Method:    vte_terminal_get_allow_bold
 */
JNIEXPORT jboolean JNICALL Java_org_gnu_gnomevte_Terminal_vte_1terminal_1get_1allow_1bold
  (JNIEnv *env, jclass klass, jobject handle)
{
	VteTerminal *term = (VteTerminal*)getPointerFromHandle(env, handle);
	return (jboolean) vte_terminal_get_allow_bold(term);
}

/*
 * Class:     org_gnu_gnomevte_Terminal
 * Method:    vte_terminal_get_has_selection
 */
JNIEXPORT jboolean JNICALL Java_org_gnu_gnomevte_Terminal_vte_1terminal_1get_1has_1selection
  (JNIEnv *env, jclass klass, jobject handle)
{
	VteTerminal *term = (VteTerminal*)getPointerFromHandle(env, handle);
	return (jboolean)vte_terminal_get_has_selection(term);
}

#if 0
/* XXX: cagney 2006-02-28: The corresponding method in the .java file
   was commented out.  */
/*
 * Class:     org_gnu_gnomevte_Terminal
 * Method:    vte_terminal_set_word_chars
 */
JNIEXPORT void JNICALL Java_org_gnu_gnomevte_Terminal_vte_1terminal_1set_1word_1chars
  (JNIEnv *env, jclass klass, jobject handle, jbyteArray spec)
{
	VteTerminal *term = (VteTerminal*)getPointerFromHandle(env, handle);
	jbyte *spec_g_byte = (*env)->GetByteArrayElements(env, spec, 0);
	const gchar *spec_g = (gchar*)spec_g_byte;
	vte_terminal_set_word_chars(term, spec_g);
	(*env)->ReleaseByteArrayElements(env, spec, spec_g_byte, 0);
}
#endif

#if 0
/* XXX: cagney 2006-02-28: The corresponding method in the .java file
   was commented out.  */
/*
 * Class:     org_gnu_gnomevte_Terminal
 * Method:    vte_terminal_is_word_char
 */
JNIEXPORT jboolean JNICALL Java_org_gnu_gnomevte_Terminal_vte_1terminal_1is_1word_1char
  (JNIEnv *env, jclass klass, jobject handle, jchar c)
{
	VteTerminal *term = (VteTerminal*)getPointerFromHandle(env, handle);
	return (jboolean)vte_terminal_is_word_char(term, (gunichar)c);
}
#endif

#if 0
/* XXX: cagney 2006-02-28: The corresponding method in the .java file
   was commented out.  */
/*
 * Class:     org_gnu_gnomevte_Terminal
 * Method:    vte_terminal_set_backspace_binding
 */
JNIEXPORT void JNICALL Java_org_gnu_gnomevte_Terminal_vte_1terminal_1set_1backspace_1binding
  (JNIEnv *env, jclass klass, jobject handle, jint binding)
{
	VteTerminal *term = (VteTerminal*)getPointerFromHandle(env, handle);
	vte_terminal_set_backspace_binding(term, (VteTerminalEraseBinding)binding);
}
#endif

#if 0
/* XXX: cagney 2006-02-28: The corresponding method in the .java file
   was commented out.  */
/*
 * Class:     org_gnu_gnomevte_Terminal
 * Method:    vte_terminal_set_delete_binding
 */
JNIEXPORT void JNICALL Java_org_gnu_gnomevte_Terminal_vte_1terminal_1set_1delete_1binding
  (JNIEnv *env, jclass klass, jobject handle, jint binding)
{
	VteTerminal *term = (VteTerminal*)getPointerFromHandle(env, handle);
	vte_terminal_set_delete_binding(term, (VteTerminalEraseBinding)binding);
}
#endif

#if 0
/* XXX: cagney 2006-02-28: The corresponding method in the .java file
   was commented out.  */
/*
 * Class:     org_gnu_gnomevte_Terminal
 * Method:    vte_terminal_set_mouse_autohide
 */
JNIEXPORT void JNICALL Java_org_gnu_gnomevte_Terminal_vte_1terminal_1set_1mouse_1autohide
  (JNIEnv *env, jclass klass, jobject handle, jboolean setting)
{
	VteTerminal *term = (VteTerminal*)getPointerFromHandle(env, handle);
	vte_terminal_set_mouse_autohide(term, (gboolean)setting);
}
#endif

#if 0
/* XXX: cagney 2006-02-28: The corresponding method in the .java file
   was commented out.  */
/*
 * Class:     org_gnu_gnomevte_Terminal
 * Method:    vte_terminal_get_mouse_autohide
 */
JNIEXPORT jboolean JNICALL Java_org_gnu_gnomevte_Terminal_vte_1terminal_1get_1mouse_1autohide
  (JNIEnv *env, jclass klass, jobject handle)
{
	VteTerminal *term = (VteTerminal*)getPointerFromHandle(env, handle);
	return vte_terminal_get_mouse_autohide(term);
}
#endif

/*
 * Class:     org_gnu_gnomevte_Terminal
 * Method:    vte_terminal_reset
 */
JNIEXPORT void JNICALL Java_org_gnu_gnomevte_Terminal_vte_1terminal_1reset
  (JNIEnv *env, jclass klass, jobject handle, jboolean full, jboolean clearHist)
{
	VteTerminal *term = (VteTerminal*)getPointerFromHandle(env, handle);
	vte_terminal_reset(term, (gboolean)full, (gboolean)clearHist);
}

#if 0
/* XXX: cagney 2006-02-28: The corresponding method in the .java file
   was commented out.  */
/*
 * Class:     org_gnu_gnomevte_Terminal
 * Method:    vte_terminal_get_cursor_position
 */
JNIEXPORT void JNICALL Java_org_gnu_gnomevte_Terminal_vte_1terminal_1get_1cursor_1position
  (JNIEnv *env, jclass klass, jobject handle, jlongArray column, jlongArray row)
{
	VteTerminal *term = (VteTerminal*)getPointerFromHandle(env, handle);
    glong *c = (glong *)(*env)->GetLongArrayElements (env, column, NULL);
    glong *r = (glong *)(*env)->GetLongArrayElements (env, row, NULL);
    vte_terminal_get_cursor_position(term, c, r);
   	(*env)->ReleaseLongArrayElements (env, column, (jlong *) c, 0);
   	(*env)->ReleaseLongArrayElements (env, row, (jlong *) r, 0);
}
#endif

#if 0
/* XXX: cagney 2006-02-28: The corresponding method in the .java file
   was commented out.  */
/*
 * Class:     org_gnu_gnomevte_Terminal
 * Method:    vte_terminal_match_clear_all
 */
JNIEXPORT void JNICALL Java_org_gnu_gnomevte_Terminal_vte_1terminal_1match_1clear_1all
  (JNIEnv *env, jclass klass, jobject handle)
{
	VteTerminal *term = (VteTerminal*)getPointerFromHandle(env, handle);
	vte_terminal_match_clear_all(term);
}
#endif

#if 0
/* XXX: cagney 2006-02-28: The corresponding method in the .java file
   was commented out.  */
/*
 * Class:     org_gnu_gnomevte_Terminal
 * Method:    vte_terminal_match_add
 */
JNIEXPORT jint JNICALL Java_org_gnu_gnomevte_Terminal_vte_1terminal_1match_1add
  (JNIEnv *env, jclass klass, jobject handle, jstring match)
{
	VteTerminal *term = (VteTerminal*)getPointerFromHandle(env, handle);
	const char *m = (const char *)(*env)->GetStringUTFChars(env, match, NULL);
	vte_terminal_match_add(term, m);
	(*env)->ReleaseStringUTFChars(env, match, m);
}
#endif

#if 0
/* XXX: cagney 2006-02-28: The corresponding method in the .java file
   was commented out.  */
/*
 * Class:     org_gnu_gnomevte_Terminal
 * Method:    vte_terminal_match_remove
 */
JNIEXPORT void JNICALL Java_org_gnu_gnomevte_Terminal_vte_1terminal_1match_1remove
  (JNIEnv *env, jclass klass, jobject handle, jint tag)
{
	VteTerminal *term = (VteTerminal*)getPointerFromHandle(env, handle);
	vte_terminal_match_remove(term, tag);
}
#endif

#if 0
/* XXX: cagney 2006-02-28: The corresponding method in the .java file
   was commented out.  */
/*
 * Class:     org_gnu_gnomevte_Terminal
 * Method:    vte_terminal_match_check
 */
JNIEXPORT jstring JNICALL Java_org_gnu_gnomevte_Terminal_vte_1terminal_1match_1check
  (JNIEnv *env, jclass klass, jobject handle, jint column, jint row, jintArray tag)
{
	VteTerminal *term = (VteTerminal*)getPointerFromHandle(env, handle);
    gint *t = (gint *)(*env)->GetIntArrayElements (env, tag, NULL);
    char* val = vte_terminal_match_check(term, column, row, t);
    (*env)->ReleaseIntArrayElements(env, tag, t, 0);
    return (*env)->NewStringUTF(env, val);
}
#endif

/*
 * Class:     org_gnu_gnomevte_Terminal
 * Method:    vte_terminal_set_emulation
 */
JNIEXPORT void JNICALL Java_org_gnu_gnomevte_Terminal_vte_1terminal_1set_1emulation
  (JNIEnv *env, jclass klass, jobject handle, jstring emulation)
{
	VteTerminal *term = (VteTerminal*)getPointerFromHandle(env, handle);
	const char* e = (const char*)(*env)->GetStringUTFChars(env, emulation, NULL);
	vte_terminal_set_emulation(term, e);
	(*env)->ReleaseStringUTFChars(env, emulation, e);
}

/*
 * Class:     org_gnu_gnomevte_Terminal
 * Method:    vte_terminal_get_emulation
 */
JNIEXPORT jstring JNICALL Java_org_gnu_gnomevte_Terminal_vte_1terminal_1get_1emulation
  (JNIEnv *env, jclass klass, jobject handle)
{
	VteTerminal *term = (VteTerminal*)getPointerFromHandle(env, handle);
	return (*env)->NewStringUTF(env, vte_terminal_get_emulation(term));
}

/*
 * Class:     org_gnu_gnomevte_Terminal
 * Method:    vte_terminal_set_encoding
 */
JNIEXPORT void JNICALL Java_org_gnu_gnomevte_Terminal_vte_1terminal_1set_1encoding
  (JNIEnv *env, jclass klass, jobject handle, jstring codeset)
{
	VteTerminal *term = (VteTerminal*)getPointerFromHandle(env, handle);
	const char* codeset_g = (const char*)(*env)->GetStringUTFChars(env, codeset, NULL);
	vte_terminal_set_encoding(term, codeset_g);
	(*env)->ReleaseStringUTFChars(env, codeset, codeset_g);
}

/*
 * Class:     org_gnu_gnomevte_Terminal
 * Method:    vte_terminal_get_encoding
 */
JNIEXPORT jstring JNICALL Java_org_gnu_gnomevte_Terminal_vte_1terminal_1get_1encoding
  (JNIEnv *env, jclass klass, jobject handle)
{
	VteTerminal *term = (VteTerminal*)getPointerFromHandle(env, handle);
	return (*env)->NewStringUTF(env, vte_terminal_get_encoding(term));
}

/*
 * Class:     org_gnu_gnomevte_Terminal
 * Method:    vte_terminal_get_status_line
 */
JNIEXPORT jstring JNICALL Java_org_gnu_gnomevte_Terminal_vte_1terminal_1get_1status_1line
  (JNIEnv *env, jclass klass, jobject handle)
{
	VteTerminal *term = (VteTerminal*)getPointerFromHandle(env, handle);
	return (*env)->NewStringUTF(env, vte_terminal_get_status_line(term));	
}

#if 0
/* XXX: cagney 2006-02-28: The corresponding method in the .java file
   was commented out.  */
/*
 * Class:     org_gnu_gnomevte_Terminal
 * Method:    vte_terminal_get_padding
 */
JNIEXPORT void JNICALL Java_org_gnu_gnomevte_Terminal_vte_1terminal_1get_1padding
  (JNIEnv *env, jclass klass, jobject handle, jintArray xpad, jintArray ypad)
{
	VteTerminal *term = (VteTerminal*)getPointerFromHandle(env, handle);
	int* x = (int*)(*env)->GetIntArrayElements(env, xpad, NULL);
	int* y = (int*)(*env)->GetIntArrayElements(env, ypad, NULL);
	vte_terminal_get_padding(term, x, y);
	(*env)->ReleaseIntArrayElements(env, xpad, x, 0);
	(*env)->ReleaseIntArrayElements(env, ypad, y, 0);
}
#endif

/*
 * Class:     org_gnu_gnomevte_Terminal
 * Method:    vte_terminal_get_adjustment
 */
JNIEXPORT jobject JNICALL Java_org_gnu_gnomevte_Terminal_vte_1terminal_1get_1adjustment
  (JNIEnv *env, jclass klass, jobject handle)
{
	VteTerminal *term = (VteTerminal*)getPointerFromHandle(env, handle);
	return getGObjectHandle(env, G_OBJECT(vte_terminal_get_adjustment(term)));
}

#if 0
/* XXX: cagney 2006-02-28: The corresponding method in the .java file
   was commented out.  */
/*
 * Class:     org_gnu_gnomevte_Terminal
 * Method:    vte_terminal_get_char_width
 */
JNIEXPORT jlong JNICALL Java_org_gnu_gnomevte_Terminal_vte_1terminal_1get_1char_1width
  (JNIEnv *env, jclass klass, jobject handle)
{
	VteTerminal *term = (VteTerminal*)getPointerFromHandle(env, handle);
	return (jlong)vte_terminal_get_char_width(term);
}
#endif

#if 0
/* XXX: cagney 2006-02-28: The corresponding method in the .java file
   was commented out.  */
/*
 * Class:     org_gnu_gnomevte_Terminal
 * Method:    vte_terminal_get_char_height
 */
JNIEXPORT jlong JNICALL Java_org_gnu_gnomevte_Terminal_vte_1terminal_1get_1char_1height
  (JNIEnv *env, jclass klass, jobject handle)
{
	VteTerminal *term = (VteTerminal*)getPointerFromHandle(env, handle);
	return (jlong)vte_terminal_get_char_height(term);
}
#endif

#if 0
/* XXX: cagney 2006-02-28: The corresponding method in the .java file
   was commented out.  */
/*
 * Class:     org_gnu_gnomevte_Terminal
 * Method:    vte_terminal_get_char_descent
 */
JNIEXPORT jlong JNICALL Java_org_gnu_gnomevte_Terminal_vte_1terminal_1get_1char_1descent
  (JNIEnv *env, jclass klass, jobject handle)
{
	VteTerminal *term = (VteTerminal*)getPointerFromHandle(env, handle);
	return (jlong)vte_terminal_get_char_descent(term);
}
#endif

#if 0
/* XXX: cagney 2006-02-28: The corresponding method in the .java file
   was commented out.  */
/*
 * Class:     org_gnu_gnomevte_Terminal
 * Method:    vte_terminal_get_char_ascent
 */
JNIEXPORT jlong JNICALL Java_org_gnu_gnomevte_Terminal_vte_1terminal_1get_1char_1ascent
  (JNIEnv *env, jclass klass, jobject handle)
{
	VteTerminal *term = (VteTerminal*)getPointerFromHandle(env, handle);
	return (jlong)vte_terminal_get_char_ascent(term);
}
#endif

#if 0
/* XXX: cagney 2006-02-28: The corresponding method in the .java file
   was commented out.  */
/*
 * Class:     org_gnu_gnomevte_Terminal
 * Method:    vte_terminal_get_row_count
 */
JNIEXPORT jlong JNICALL Java_org_gnu_gnomevte_Terminal_vte_1terminal_1get_1row_1count
  (JNIEnv *env, jclass klass, jobject handle)
{
	VteTerminal *term = (VteTerminal*)getPointerFromHandle(env, handle);
	return (jlong)vte_terminal_get_row_count(term);
}
#endif

#if 0
/* XXX: cagney 2006-02-28: The corresponding method in the .java file
   was commented out.  */
/*
 * Class:     org_gnu_gnomevte_Terminal
 * Method:    vte_terminal_get_column_count
 */
JNIEXPORT jlong JNICALL Java_org_gnu_gnomevte_Terminal_vte_1terminal_1get_1column_1count
  (JNIEnv *env, jclass klass, jobject handle)
{
	VteTerminal *term = (VteTerminal*)getPointerFromHandle(env, handle);
	return (jlong)vte_terminal_get_column_count(term);
}
#endif

#if 0
/* XXX: cagney 2006-02-28: The corresponding method in the .java file
   was commented out.  */
/*
 * Class:     org_gnu_gnomevte_Terminal
 * Method:    vte_terminal_get_window_title
 */
JNIEXPORT jstring JNICALL Java_org_gnu_gnomevte_Terminal_vte_1terminal_1get_1window_1title
  (JNIEnv *env, jclass klass, jobject handle)
{
	VteTerminal *term = (VteTerminal*)getPointerFromHandle(env, handle);
	return (*env)->NewStringUTF(env, vte_terminal_get_window_title(term));
}
#endif

#if 0
/* XXX: cagney 2006-02-28: The corresponding method in the .java file
   was commented out.  */
/*
 * Class:     org_gnu_gnomevte_Terminal
 * Method:    vte_terminal_get_icon_title
 */
JNIEXPORT jstring JNICALL Java_org_gnu_gnomevte_Terminal_vte_1terminal_1get_1icon_1title
  (JNIEnv *env, jclass klass, jobject handle)
{
	VteTerminal *term = (VteTerminal*)getPointerFromHandle(env, handle);
	return (*env)->NewStringUTF(env, vte_terminal_get_icon_title(term));
}
#endif

#ifdef __cplusplus
}
#endif
#endif
