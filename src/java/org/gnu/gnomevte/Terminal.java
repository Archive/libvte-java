/*
 * Java-Gnome Bindings Library
 *
 * Copyright 1998-2004 the Java-Gnome Team, all rights reserved.
 *
 * The Java-Gnome bindings library is free software distributed under
 * the terms of the GNU Library General Public License version 2.
 */


package org.gnu.gnomevte;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.Iterator;
import java.util.Vector;

import org.gnu.gdk.Color;
import org.gnu.gdk.Pixbuf;
import org.gnu.glib.Boxed;
import org.gnu.glib.EventMap;
import org.gnu.glib.Handle;
import org.gnu.gnomevte.event.*;
import org.gnu.gtk.Adjustment;
import org.gnu.pango.FontDescription;

/**
 * VteTerminal widget bindings implementation.
 *
 * @deprecated This class is part of the java-gnome 2.x family of libraries,
 *             which, due to their inefficiency and complexity, are no longer
 *             being maintained and have been abandoned by the java-gnome
 *             project. This class may have an equivalent
 *             in java-gnome 4.0; have a look for
 *             <code>org.gnome.vte.Terminal</code>.
 */
public class Terminal extends org.gnu.gtk.Widget {
	
	private int pid_t;
	
    /**
     * Listeners for handling generic terminal events.
     * @deprecated Superceeded by java-gnome 4.0; this method may or may not
     *             exist in the new bindings but if it does, it will likely have 
     *             a different name or signature in order that the presented API
     *             is a more algorithmic mapping of the underlying native libraries.
     */
    private Vector terminalListeners = null;

    /**
     * Listeners for handling terminal commit events.
     * @deprecated Superceeded by java-gnome 4.0; this method may or may not
     *             exist in the new bindings but if it does, it will likely have 
     *             a different name or signature in order that the presented API
     *             is a more algorithmic mapping of the underlying native libraries.
     */
    private Vector commitListeners = null;

    /**
     * Listeners for handling terminal character size change events.
     * @deprecated Superceeded by java-gnome 4.0; this method may or may not
     *             exist in the new bindings but if it does, it will likely have 
     *             a different name or signature in order that the presented API
     *             is a more algorithmic mapping of the underlying native libraries.
     */
    private Vector characterSizeChangeListeners = null;

    /**
     * Listeners for handling terminal move window events.
     * @deprecated Superceeded by java-gnome 4.0; this method may or may not
     *             exist in the new bindings but if it does, it will likely have 
     *             a different name or signature in order that the presented API
     *             is a more algorithmic mapping of the underlying native libraries.
     */
    private Vector moveWindowListeners = null;

    /**
     * Listeners for handling terminal resize window events.
     * @deprecated Superceeded by java-gnome 4.0; this method may or may not
     *             exist in the new bindings but if it does, it will likely have 
     *             a different name or signature in order that the presented API
     *             is a more algorithmic mapping of the underlying native libraries.
     */
    private Vector resizeWindowListeners = null;

    /**
     * Listeners for handling terminal text scrolled events.
     * @deprecated Superceeded by java-gnome 4.0; this method may or may not
     *             exist in the new bindings but if it does, it will likely have 
     *             a different name or signature in order that the presented API
     *             is a more algorithmic mapping of the underlying native libraries.
     */
    private Vector textScrolledListeners = null;
    
	/**
	 * Used in the bindings development.
	 * You should not use this constructor
	 * Use <code>Terminal ()</code> or <code>Terminal (command, args, workingDir)</code> instead.
	 */
	protected Terminal (Handle handle){
		super (handle);
	}

	/**
	 * Creates a new Termina widget.
	 * This constructor launchs a shell inside de widget.
	 */
	public Terminal (){
		super (vte_terminal_new ());
	}

	/**
	 * Creates a new Terminal widget and executes <code>command</code> inside it.
	 *
	 * @param command Command launched inside the terminal.
	 * @param args arguments passed to the <code>command</code>.
	 * @param workingDir working directory passed to the process.
	 */
	public Terminal (String command, String args[], String workingDir){
		this();
		pid_t = forkCommand (command, args, workingDir, true, true, true);
	}

	/**
	 * Creates an new Terminal widget and launches a shell inside it.
	 *
	 * @return The widget with a shell in it.
	 */
	public static Terminal terminalAndShell (){
		Terminal t = new Terminal (null, null, null);
		return t;
	}

		
	/**
	 * Forks the <code>command</code> and show it inside the widget.
	 * 
	 * @param command program to launch in a new process.
	 * @param args The argument list available to the executed program.
	 * @param directory working directory available to the executed program.
	 * FIXME: investigate the meaning of laslog, utmp, wtmp. 
	 * @return Integer representing the process id.
	 */
	public int forkCommand (String command, String args[], String directory, boolean lastlog, boolean utmp, boolean wtmp){
		if(args == null)
			args = new String[]{};
		return vte_terminal_fork_command (getHandle(), command, args, directory, lastlog, utmp, wtmp);
	}

 	/** 
	 * Associate a pty file descriptor with the terminal
	 * 
	 * @param pty_master pty fd
	 */
	public void setPty (int pty_master){
	    vte_terminal_set_pty (getHandle(), pty_master);
	}

	/** 
	 * Send data to the terminal to display to handle in some way. 
	 * If data is null, it will be ignored.
	 * 
	 * @param data Sent to the terminal.
	 */
	public void feed (String data){
		if (data != null)
			vte_terminal_feed (getHandle(), data, data.length ());
	}
	
	/**	
	 * Send data to the terminal's forked command to handle in some way.
	 * If data is null, it will be ignored.
	 * 
	 * @param data Sent to the terminal.
	 */
	public void feedChild (String data){
		if (data != null){
			vte_terminal_feed_child (getHandle(), data, data.length ());
		}
	}

	/**
	 * Copy currently selected text to the clipboard.
	 */
	public void copyClipboard (){
		vte_terminal_copy_clipboard (getHandle());
	}

	/**
	 * Paste clipboard text from the clipboard to the terminal.
	 */
	public void pasteClipboard (){
		vte_terminal_paste_clipboard (getHandle());
	}

	/*
	 * TODO: Test!!!
	 */	 
	public void copyPrimary (){
		vte_terminal_copy_primary(getHandle());
	}

	/*
	 * TODO: Test!!!
	 */
	public void pastePrimary (){
		vte_terminal_paste_primary(getHandle());
	}

	/**
	 * Set the terminal's size.
	 *
	 * @param columns The terminal's width.
	 * @param rows The terminal's height.
	 */
	public void setSize (int columns, int rows){
		vte_terminal_set_size (getHandle(), columns, rows);
	}
	
	/**
	 * Set the terminal's audible bell.
	 *
	 * @param audible  If true, the terminal emits a Bip to attract users attention.
	 */
	public void setAudibleBell (boolean audible){
		vte_terminal_set_audible_bell (getHandle(), audible);
	}

	/**
	 *  Get the terminal's audible bell state.
	 *
	 *  @return The audible bell state.
	 */
	public boolean isBellAudible (){
		return vte_terminal_get_audible_bell (getHandle());
	}

	/**
	 * Set the terminal's visible bell state.
	 * 
	 * @param visible  If true, the terminal blinks to attract users attention.
	 */
	public void setVisibleBell (boolean visible){
		vte_terminal_set_visible_bell (getHandle(), visible);
	}

	/**
	 *  Get the terminal's audible bell state.
	 *
	 *  @return The visual bell state
	 */
	public boolean isBellVisible (){
		return vte_terminal_get_visible_bell (getHandle());
	}

	/**
	 * If true, scrolls the widget down following the output.
	 *
	 * @param scrollOnOutput If true, scroll.
	 */
	public void setScrollOnOutput (boolean scrollOnOutput){
		vte_terminal_set_scroll_on_output (getHandle(), scrollOnOutput);
	}

	/**
	 * If true, scrolls the widget down when pressing following the keyboard press.
	 *
	 * @param scrollOnKeystroke If true, scroll.
	 */
	public void setScrollOnKeystroke (boolean scrollOnKeystroke){
		vte_terminal_set_scroll_on_keystroke (getHandle(), scrollOnKeystroke);
	}

	/**
	 * Restores the default colors.
	 * 
	 */
	public void setDefaultColors() {
		vte_terminal_set_default_colors(getHandle());
	}
	 
	/**
	 * Set the <code>file</code> as the background image of the terminal.
	 *
	 * @param file File to set as background.
	 * 
	 * @throws FileNotFoundException if the image doesn't exist.
	 */
	public void setBackgroundImage(String file) throws FileNotFoundException{
		File f = new File(file);
		if (f.exists())
			vte_terminal_set_background_image_file(getHandle(), file);
		else
			throw new FileNotFoundException();
	}
		
    public void setBackgroudImage(String file) throws FileNotFoundException{
	setBackgroundImage(file);
    }

	/**
	 * Sets the background image for the widget.  Text which would otherwise 
	 * be drawn using the default background color will instead be drawn 
	 * over the specified image. If necessary, the image will be tiled to 
	 * cover the widget's entire visible area.
	 * 
	 * @param image
	 */
	public void setBackgroundImage(Pixbuf image) {
		vte_terminal_set_background_image(getHandle(), image.getHandle());
	}
		
	/**
	 * Set whether or not the cursor blinks.
	 *
	 * @param blinks If true, blinks.
	 */
	public void setCursorBlinks(boolean blinks){
		vte_terminal_set_cursor_blinks(getHandle(), blinks);
	}
	
	/**
	 * Set the number of scrollback lines.
	 *
	 * @param lines The number of lines to save in the buffer.
	 */
	public void setScrollbackLines(int lines){
		vte_terminal_set_scrollback_lines(getHandle(), lines);
	}
		
	/**
	 * Sets the terminal's background saturation level.
	 *
	 * @param saturation The saturation level.
	 */
	public void setBackgroundSaturation (int saturation){
		vte_terminal_set_background_saturation (getHandle(), saturation);
	}

	/**
	 * Sets the terminal backgroud transparent or not.
	 *
	 * @param transparent Transparent if <code>true</code>.
	 */
	public void setBackgroundTransparent (boolean transparent){
		vte_terminal_set_background_transparent (getHandle(), transparent);
	}
	
	/**
	 * Sets the background color for text which does not have a specific 
	 * background color assigned. Only has effect when no background image 
	 * is set and when the terminal is not transparent.
	 * @param color
	 */
	public void setBackgroundColor(Color color) {
		vte_terminal_set_color_background(getHandle(), color.getHandle());
	}	

    public void setBackgroudColor(Color color) {
	setBackgroundColor(color);
    }

	/**
	 * Sets the foreground color used to draw normal text
	 * @param color
	 */
	public void setForegroundColor(Color color) {
		vte_terminal_set_color_foreground(getHandle(), color.getHandle());
	}

	/**
	 * The terminal widget uses a 28-color model comprised of the default 
	 * foreground and background colors, the bold foreground color, the 
	 * dim foreground color, an eight color palette, and bold versions of 
	 * the eight color palette, and a dim version of the the eight color 
	 * palette.
	 * 
	 * size must be either 0, 8, 16, or 24. If foreground is null and 
	 * size is greater than 0, the new foreground color is taken from 
	 * palette[7]. If background is null and size is greater than 0, 
	 * the new background color is taken from palette[0]. If size is 
	 * 8 or 16, the third (dim) and possibly second (bold) 8-color 
	 * palette is extrapolated from the new background color and the 
	 * items in palette.
	 * 
	 * @param fgcolor
	 * @param bgcolor
	 * @param palett
	 * @param size
	 */
	public void setColors(Color fgcolor, Color bgcolor, Color palett, int size) {
		vte_terminal_set_colors(getHandle(), fgcolor.getHandle(), 
							bgcolor.getHandle(), palett.getHandle(), size);
	}

	/**
	 * Sets the color used to draw bold text in the default foreground color.
	 * @param color
	 */
	public void setColorBold(Color color) {
		vte_terminal_set_color_bold(getHandle(), color.getHandle());
	}
	
	/**
	 * Sets the color used to draw dim text in the default foreground color.
	 * 
	 * @param color
	 */
	public void setColorDim(Color color) {
		vte_terminal_set_color_dim(getHandle(), color.getHandle());
	}

	/**
	 * Sets the font used for rendering all text displayed by the terminal. 
	 * The terminal will immediately attempt to load the desired font, 
	 * retrieve its metrics, and attempts to resize itself to keep the same 
	 * number of rows and columns.
	 */
	public void setFont(FontDescription font) {
		vte_terminal_set_font(getHandle(), font.getHandle());
	}

	/**
	 * Queries the terminal for information about the fonts which will be 
	 * used to draw text in the terminal.
	 * @return FontDescription
	 */
	public FontDescription getFont() {
		Handle handle = vte_terminal_get_font(getHandle());
        if (handle != null ) {
            FontDescription obj = (FontDescription) Boxed.getBoxedFromHandle(handle);
            if (obj != null) {
                return obj;
            } else {
                return new FontDescription(obj);
            }
        }
        return null;
	}

	/**
	 * Controls whether or not the terminal will attempt to draw bold 
	 * text by repainting text with a different offset.
	 * @param allowBold
	 */
	public void setAllowBold(boolean allowBold) {
		vte_terminal_set_allow_bold(getHandle(), allowBold);
	}
	
	/**
	 * Checks whether or not the terminal will attempt to draw bold text 
	 * by repainting text with a one-pixel offset.
	 * @return boolean
	 */
	public boolean getAllowBold() {
		return vte_terminal_get_allow_bold(getHandle());
	}
	
	/**
	 * Resets as much of the terminal's internal state as possible, 
	 * discarding any unprocessed input data, resetting character 
	 * attributes, cursor state, national character set state, status 
	 * line, terminal modes (insert/delete), selection state, and encoding.
	 * 
	 * @param full true to reset tabstops
	 * @param clearHistory true to empty the terminal's scrollback buffer
	 */
	public void reset(boolean full, boolean clearHistory) {
		vte_terminal_reset(getHandle(), full, clearHistory);
	}
	
	/**
	 * Sets what type of terminal the widget attempts to emulate by scanning 
	 * for control sequences defined in the system's termcap file. Unless 
	 * you are interested in this feature, always use "xterm".
	 * @param emulation
	 */
	public void setEmulation(String emulation) {
		vte_terminal_set_emulation(getHandle(), emulation);
	}
	
	/**
	 * Queries the terminal for its current emulation, as last set by a 
	 * call to setEmulation().
	 * @return String
	 */
	public String getEmulation() {
		return vte_terminal_get_emulation(getHandle());
	}
	
	/**
	 * Changes the encoding the terminal will expect data from the child 
	 * to be encoded with. For certain terminal types, applications 
	 * executing in the terminal can change the encoding. The default 
	 * encoding is defined by the application's locale settings.
	 * @param codeset
	 */
	public void setEncoding(String codeset) {
		vte_terminal_set_encoding(getHandle(), codeset);
	}
	
	/**
	 * Determines the name of the encoding in which the terminal expects 
	 * data to be encoded.
	 * @return String
	 */
	public String getEncoding() {
		return vte_terminal_get_encoding(getHandle());
	}
	
	/**
	 * Some terminal emulations specify a status line which is separate from 
	 * the main display area, and define a means for applications to move the 
	 * cursor to the status line and back.
	 * 
	 * @return the current contents of the terminal's status line. 
	 *         For terminals like "xterm", this will usually be the 
	 *         empty string.
	 */
	public String getStatusLine() {
		return vte_terminal_get_status_line(getHandle());
	}
	
	/** 
	 * Checks if the terminal currently contains selected text. 
	 * Note that this is different from determining if the terminal 
	 * is the owner of any GtkClipboard items. 
	 * @return <code>true</code> if the terminal has a selection.
	 */
	public boolean hasSelection() {
		return vte_terminal_get_has_selection(getHandle());
	}
    
    /**
     * Get the {@link Adjustment} which is the "model" object for Terminal.
     * @return The adjustment model
     * @deprecated Superceeded by java-gnome 4.0; this method may or may not
     *             exist in the new bindings but if it does, it will likely have 
     *             a different name or signature in order that the presented API
     *             is a more algorithmic mapping of the underlying native libraries.
     */
    public Adjustment getAdjustment() {
        Handle hndl = vte_terminal_get_adjustment(getHandle());
        return Adjustment.getAdjustment(hndl);
    }
	
	static {
		System.loadLibrary ("vtejni-" + Config.VTE_API_VERSION);
	}
	
	// --- Event Handling ---

	private static EventMap evtMap = new EventMap();
	static {
		addEvents(evtMap);
	}
	
	
	
	/**
	 * This method invokes all the TerminalListeners that are registered
	 * with this instance of a Terminal.
	 * @param event the event to fire.
	 */
	protected void fireTerminalEvent(TerminalEvent event) {
		// Skip if there are no listeners
		if (this.terminalListeners == null) {
			return;
		}
		
		// Invoke all the listeners
		for (final Iterator iterator = this.terminalListeners.iterator(); iterator.hasNext();) {
			TerminalListener listener = (TerminalListener) iterator.next();
			listener.terminalEvent(event);
		}
	}

	/**
	 * This method invokes all the TerminalCommitListeners that are registered
	 * with this instance of a Terminal.
	 * @param event the event to fire.
	 */
	protected void fireTerminalCommitEvent(CommitEvent event) {
		// Skip if there are no listeners
		if (this.commitListeners == null) {
			return;
		}
		
		// Invoke all the listeners
		for (final Iterator iterator = this.commitListeners.iterator(); iterator.hasNext();) {
			CommitListener listener = (CommitListener) iterator.next();
			listener.committed(event);
		}
	}

	/**
	 * This method invokes all the TerminalCharacterSizeChangedListener that are registered
	 * with this instance of a Terminal.
	 * @param event the event to fire.
	 */
	protected void fireTerminalCharacterSizeChangedEvent(CharacterSizeChangedEvent event) {
		// Skip if there are no listeners
		if (this.characterSizeChangeListeners == null) {
			return;
		}
		
		// Invoke all the listeners
		for (final Iterator iterator = this.characterSizeChangeListeners.iterator(); iterator.hasNext();) {
			CharacterSizeChangedListener listener = (CharacterSizeChangedListener) iterator.next();
			listener.characterSizeChanged(event);
		}
	}

	/**
	 * This method invokes all the TerminalMoveWindowListener that are registered
	 * with this instance of a Terminal.
	 * @param event the event to fire.
	 */
	protected void fireTerminalMoveWindowEvent(MoveWindowEvent event) {
		// Skip if there are no listeners
		if (this.moveWindowListeners == null) {
			return;
		}
		
		// Invoke all the listeners
		for (final Iterator iterator = this.moveWindowListeners.iterator(); iterator.hasNext();) {
			MoveWindowListener listener = (MoveWindowListener) iterator.next();
			listener.windowMoved(event);
		}
	}

	/**
	 * This method invokes all the TerminalResizeWindowListeners that are registered
	 * with this instance of a Terminal.
	 * @param event the event to fire.
	 */
	protected void fireTerminalResizeWindowEvent(ResizeWindowEvent event) {
		// Skip if there are no listeners
		if (this.resizeWindowListeners == null) {
			return;
		}
		
		// Invoke all the listeners
		for (final Iterator iterator = this.resizeWindowListeners.iterator(); iterator.hasNext();) {
			ResizeWindowListener listener = (ResizeWindowListener) iterator.next();
			listener.windowResized(event);
		}
	}

	/**
	 * This method invokes all the TerminalTextScrolledListeners that are registered
	 * with this instance of a Terminal.
	 * @param event the event to fire.
	 */
	protected void fireTerminalTextScrolledEvent(TextScrolledEvent event) {
		// Skip if there are no listeners
		if (this.textScrolledListeners == null) {
			return;
		}
		
		// Invoke all the listeners
		for (final Iterator iterator = this.textScrolledListeners.iterator(); iterator.hasNext();) {
			TextScrolledListener listener = (TextScrolledListener) iterator.next();
			listener.textScrolled(event);
		}
	}

	/**
	 * Registers a TerminalListener to handle generic terminal events.
	 * @param listener the listener to register.
	 */
	public void addListener(TerminalListener listener) {
		// Don't add the listener a second time if it is in the Vector.
		int i = findListener(this.terminalListeners, listener);
		if (i == -1) {
			if (null == this.terminalListeners) {
				evtMap.initialize(this, TerminalEvent.Type.CHILD_EXITED);
				evtMap.initialize(this, TerminalEvent.Type.SELECTION_CHANGED);
				evtMap.initialize(this, TerminalEvent.Type.CONTENTS_CHANGED);
				evtMap.initialize(this, TerminalEvent.Type.CURSOR_MOVED);
				evtMap.initialize(this, TerminalEvent.Type.DECREASE_FONT_SIZE);
				evtMap.initialize(this, TerminalEvent.Type.DEICONIFY_WINDOW);
				evtMap.initialize(this, TerminalEvent.Type.EMULATION_CHANGED);
				evtMap.initialize(this, TerminalEvent.Type.ENCODING_CHANGED);
				evtMap.initialize(this, TerminalEvent.Type.EOF);
				evtMap.initialize(this, TerminalEvent.Type.ICON_TITLE_CHANGED);
				evtMap.initialize(this, TerminalEvent.Type.ICONIFY_WINDOW);
				evtMap.initialize(this, TerminalEvent.Type.INCREASE_FONT_SIZE);
				evtMap.initialize(this, TerminalEvent.Type.LOWER_WINDOW);
				evtMap.initialize(this, TerminalEvent.Type.MAXIMIZE_WINDOW);
				evtMap.initialize(this, TerminalEvent.Type.RAISE_WINDOW);
				evtMap.initialize(this, TerminalEvent.Type.REFRESH_WINDOW);
				evtMap.initialize(this, TerminalEvent.Type.RESTORE_WINDOW);
				evtMap.initialize(this, TerminalEvent.Type.STATUS_LINE_CHANGED);
				evtMap.initialize(this, TerminalEvent.Type.TEXT_DELETED);
				evtMap.initialize(this, TerminalEvent.Type.TEXT_INSERTED);
				evtMap.initialize(this, TerminalEvent.Type.TEXT_MODIFIED);
				evtMap.initialize(this, TerminalEvent.Type.WINDOW_TITLE_CHANGED);

				this.terminalListeners = new Vector();
			}
			this.terminalListeners.addElement(listener);
		}
	}
	
	/**
	 * Removes the given TerminalListener.
	 * @param listener the listener to remove.
	 */
	public void removeListener(TerminalListener listener) {
		int i = findListener(this.terminalListeners, listener);
		if (i > -1) {
			this.terminalListeners.remove(i);
		}
		if (0 == this.terminalListeners.size()) {
			evtMap.uninitialize(this, TerminalEvent.Type.CHILD_EXITED);
			evtMap.uninitialize(this, TerminalEvent.Type.SELECTION_CHANGED);
			evtMap.uninitialize(this, TerminalEvent.Type.CONTENTS_CHANGED);
			evtMap.uninitialize(this, TerminalEvent.Type.CURSOR_MOVED);
			evtMap.uninitialize(this, TerminalEvent.Type.DECREASE_FONT_SIZE);
			evtMap.uninitialize(this, TerminalEvent.Type.DEICONIFY_WINDOW);
			evtMap.uninitialize(this, TerminalEvent.Type.EMULATION_CHANGED);
			evtMap.uninitialize(this, TerminalEvent.Type.ENCODING_CHANGED);
			evtMap.uninitialize(this, TerminalEvent.Type.EOF);
			evtMap.uninitialize(this, TerminalEvent.Type.ICON_TITLE_CHANGED);
			evtMap.uninitialize(this, TerminalEvent.Type.ICONIFY_WINDOW);
			evtMap.uninitialize(this, TerminalEvent.Type.INCREASE_FONT_SIZE);
			evtMap.uninitialize(this, TerminalEvent.Type.LOWER_WINDOW);
			evtMap.uninitialize(this, TerminalEvent.Type.MAXIMIZE_WINDOW);
			evtMap.uninitialize(this, TerminalEvent.Type.RAISE_WINDOW);
			evtMap.uninitialize(this, TerminalEvent.Type.REFRESH_WINDOW);
			evtMap.uninitialize(this, TerminalEvent.Type.RESTORE_WINDOW);
			evtMap.uninitialize(this, TerminalEvent.Type.STATUS_LINE_CHANGED);
			evtMap.uninitialize(this, TerminalEvent.Type.TEXT_DELETED);
			evtMap.uninitialize(this, TerminalEvent.Type.TEXT_INSERTED);
			evtMap.uninitialize(this, TerminalEvent.Type.TEXT_MODIFIED);
			evtMap.uninitialize(this, TerminalEvent.Type.WINDOW_TITLE_CHANGED);

			this.terminalListeners = null;
		}
	}


	/**
	 * Registers a TerminalCommitListener to handle terminal commit events.
	 * @param listener the listener to register.
	 */
	public void addListener(CommitListener listener) {
		// Don't add the listener a second time if it is in the Vector.
		int i = findListener(this.commitListeners, listener);
		if (i == -1) {
			if (null == this.commitListeners) {
				evtMap.initialize(this, CommitEvent.Type.COMMIT);

				this.commitListeners = new Vector();
			}
			this.commitListeners.addElement(listener);
		}
	}
	
	/**
	 * Removes the given TerminalCommitListener.
	 * @param listener the listener to remove.
	 */
	public void removeListener(CommitListener listener) {
		int i = findListener(this.commitListeners, listener);
		if (i > -1) {
			this.commitListeners.remove(i);
		}
		if (0 == this.commitListeners.size()) {
			evtMap.uninitialize(this, CommitEvent.Type.COMMIT);

			this.commitListeners = null;
		}
	}


	/**
	 * Registers a TerminalCharacterSizeChangedListener to handle terminal character 
	 * size change events.
	 * @param listener the listener to register.
	 */
	public void addListener(CharacterSizeChangedListener listener) {
		// Don't add the listener a second time if it is in the Vector.
		int i = findListener(this.characterSizeChangeListeners, listener);
		if (i == -1) {
			if (null == this.characterSizeChangeListeners) {
				evtMap.initialize(this, CharacterSizeChangedEvent.Type.CHAR_SIZE_CHANGED);

				this.characterSizeChangeListeners = new Vector();
			}
			this.characterSizeChangeListeners.addElement(listener);
		}
	}
	
	/**
	 * Removes the given TerminalCharacterSizeChangedListener.
	 * @param listener the listener to remove.
	 */
	public void removeListener(CharacterSizeChangedListener listener) {
		int i = findListener(this.characterSizeChangeListeners, listener);
		if (i > -1) {
			this.characterSizeChangeListeners.remove(i);
		}

		if (0 == this.characterSizeChangeListeners.size()) {
			evtMap.uninitialize(this, CharacterSizeChangedEvent.Type.CHAR_SIZE_CHANGED);

			this.characterSizeChangeListeners = null;
		}
	}

	/**
	 * Registers a TerminalMoveWindowListener to handle terminal move window events.
	 * @param listener the listener to register.
	 */
	public void addListener(MoveWindowListener listener) {
		// Don't add the listener a second time if it is in the Vector.
		int i = findListener(this.moveWindowListeners, listener);
		if (i == -1) {
			if (null == this.moveWindowListeners) {
				evtMap.initialize(this, MoveWindowEvent.Type.MOVE_WINDOW);

				this.moveWindowListeners = new Vector();
			}
			this.moveWindowListeners.addElement(listener);
		}
	}
	
	/**
	 * Removes the given TerminalMoveWindowListener.
	 * @param listener the listener to remove.
	 */
	public void removeListener(MoveWindowListener listener) {
		int i = findListener(this.moveWindowListeners, listener);
		if (i > -1) {
			this.moveWindowListeners.remove(i);
		}
		if (0 == this.moveWindowListeners.size()) {
			evtMap.uninitialize(this, MoveWindowEvent.Type.MOVE_WINDOW);

			this.moveWindowListeners = null;
		}
	}

	/**
	 * Registers a TerminalResizeWindowListener to handle terminal resize window events.
	 * @param listener the listener to register.
	 */
	public void addListener(ResizeWindowListener listener) {
		// Don't add the listener a second time if it is in the Vector.
		int i = findListener(this.resizeWindowListeners, listener);
		if (i == -1) {
			if (null == this.resizeWindowListeners) {
				evtMap.initialize(this, ResizeWindowEvent.Type.RESIZE_WINDOW);

				this.resizeWindowListeners = new Vector();
			}
			this.resizeWindowListeners.addElement(listener);
		}
	}
	
	/**
	 * Removes the given TerminalResizeWindowListener.
	 * @param listener the listener to remove.
	 */
	public void removeListener(ResizeWindowListener listener) {
		int i = findListener(this.resizeWindowListeners, listener);
		if (i > -1) {
			this.resizeWindowListeners.remove(i);
		}
		if (0 == this.resizeWindowListeners.size()) {
			evtMap.uninitialize(this, ResizeWindowEvent.Type.RESIZE_WINDOW);

			this.resizeWindowListeners = null;
		}
	}

	/**
	 * Registers a TerminalTextScrolledListener to handle terminal text scroll events.
	 * @param listener the listener to register.
	 */
	public void addListener(TextScrolledListener listener) {
		// Don't add the listener a second time if it is in the Vector.
		int i = findListener(this.textScrolledListeners, listener);
		if (i == -1) {
			if (null == this.textScrolledListeners) {
				evtMap.initialize(this, TextScrolledEvent.Type.TEXT_SCROLLED);

				this.textScrolledListeners = new Vector();
			}
			this.textScrolledListeners.addElement(listener);
		}
	}
	
	/**
	 * Removes the given TerminalTextScrolledListener.
	 * @param listener the listener to remove.
	 */
	public void removeListener(TextScrolledListener listener) {
		int i = findListener(this.textScrolledListeners, listener);
		if (i > -1) {
			this.textScrolledListeners.remove(i);
		}
		if (0 == this.textScrolledListeners.size()) {
			evtMap.uninitialize(this, TextScrolledEvent.Type.TEXT_SCROLLED);

			this.textScrolledListeners = null;
		}
	}
	
    /**
     * Called when the command executed through vte_terminal_fork_command is
     * no longer running.
     * This method will automatically fire a TerminalEvent.
     * @deprecated Superceeded by java-gnome 4.0; this method may or may not
     *             exist in the new bindings but if it does, it will likely have 
     *             a different name or signature in order that the presented API
     *             is a more algorithmic mapping of the underlying native libraries.
     */
    private void handleChildExited() {
        fireTerminalEvent(new TerminalEvent(this, TerminalEvent.Type.CHILD_EXITED));
    }
    
    /**
     * Called when the Terminal has it's selection changed.
     * This method will automatically fire a TerminalEvent.
     * @deprecated Superceeded by java-gnome 4.0; this method may or may not
     *             exist in the new bindings but if it does, it will likely have 
     *             a different name or signature in order that the presented API
     *             is a more algorithmic mapping of the underlying native libraries.
     */
    private void handleSelectionChanged() {
        fireTerminalEvent(new TerminalEvent(this, TerminalEvent.Type.SELECTION_CHANGED));
    }
    
    /**
     * Called when the user has submmitted data to the Terminal.
     * This method will automatically fire a TerminalCommitEvent.
     * @param text the text that's being commited.
     * @param length the length of the text recieved.
     * @deprecated Superceeded by java-gnome 4.0; this method may or may not
     *             exist in the new bindings but if it does, it will likely have 
     *             a different name or signature in order that the presented API
     *             is a more algorithmic mapping of the underlying native libraries.
     */
    private void handleCommit(String text, int length) {
        
        // Create the event
        CommitEvent event = new CommitEvent(this,
                CommitEvent.Type.COMMIT, text);
        
        // Fire the event
        fireTerminalCommitEvent(event);
    }
    
    /**
     * Called whenever selection of a new font causes the values of the 
     * character width or character height fields to change.
     * This method will automatically fire a TerminalCharacterSizeChangedEvent.
     * @param width the new character cell width.
     * @param height the new character cell height. 
     * @deprecated Superceeded by java-gnome 4.0; this method may or may not
     *             exist in the new bindings but if it does, it will likely have 
     *             a different name or signature in order that the presented API
     *             is a more algorithmic mapping of the underlying native libraries.
     */
    private void handleCharSizeChanged(int width, int height) {

        // Create the event
        CharacterSizeChangedEvent event = new CharacterSizeChangedEvent(
                this, CharacterSizeChangedEvent.Type.CHAR_SIZE_CHANGED,
                width, height);
        
        // Fire the event
        fireTerminalCharacterSizeChangedEvent(event);
    }
    
    /**
     * Called whenever the visible appearance of the Terminal have changed.
     * This method will automatically fire a TerminalEvent.
     * @deprecated Superceeded by java-gnome 4.0; this method may or may not
     *             exist in the new bindings but if it does, it will likely have 
     *             a different name or signature in order that the presented API
     *             is a more algorithmic mapping of the underlying native libraries.
     */
    private void handleContentsChanged() {
        fireTerminalEvent(new TerminalEvent(this, TerminalEvent.Type.CONTENTS_CHANGED));
    }
    
    /**
     * Called whenever the cursor moves to a new character cell.
     * This method will automatically fire a TerminalEvent.
     * @deprecated Superceeded by java-gnome 4.0; this method may or may not
     *             exist in the new bindings but if it does, it will likely have 
     *             a different name or signature in order that the presented API
     *             is a more algorithmic mapping of the underlying native libraries.
     */
    private void handleCursorMoved() {
        fireTerminalEvent(new TerminalEvent(this, TerminalEvent.Type.CURSOR_MOVED));
    }
    
    /**
     * Called when the user hits the '-' key while holding the Control key.
     * @deprecated Superceeded by java-gnome 4.0; this method may or may not
     *             exist in the new bindings but if it does, it will likely have 
     *             a different name or signature in order that the presented API
     *             is a more algorithmic mapping of the underlying native libraries.
     */
    private void handleDecreaseFontSize() {
        fireTerminalEvent(new TerminalEvent(this, TerminalEvent.Type.DECREASE_FONT_SIZE));
    }

    /**
     * Emitted at the child application's request.
     * @deprecated Superceeded by java-gnome 4.0; this method may or may not
     *             exist in the new bindings but if it does, it will likely have 
     *             a different name or signature in order that the presented API
     *             is a more algorithmic mapping of the underlying native libraries.
     */
    private void handleDeiconifyWindow() {
        fireTerminalEvent(new TerminalEvent(this, TerminalEvent.Type.DEICONIFY_WINDOW));
    }

    /**
     * Emitted when the terminal emulation changes.
     * @deprecated Superceeded by java-gnome 4.0; this method may or may not
     *             exist in the new bindings but if it does, it will likely have 
     *             a different name or signature in order that the presented API
     *             is a more algorithmic mapping of the underlying native libraries.
     */
    private void handleEmulationChanged() {
        fireTerminalEvent(new TerminalEvent(this, TerminalEvent.Type.EMULATION_CHANGED));
    }

    /**
     * Emitted when the encoding of the terminal changes.
     * @deprecated Superceeded by java-gnome 4.0; this method may or may not
     *             exist in the new bindings but if it does, it will likely have 
     *             a different name or signature in order that the presented API
     *             is a more algorithmic mapping of the underlying native libraries.
     */
    private void handleEncodingChanged() {
        fireTerminalEvent(new TerminalEvent(this, TerminalEvent.Type.ENCODING_CHANGED));
    }

    /**
     * Emitted when an End-Of-File is issued to the terminal.
     * @deprecated Superceeded by java-gnome 4.0; this method may or may not
     *             exist in the new bindings but if it does, it will likely have 
     *             a different name or signature in order that the presented API
     *             is a more algorithmic mapping of the underlying native libraries.
     */
    private void handleEof() {
        fireTerminalEvent(new TerminalEvent(this, TerminalEvent.Type.EOF));
    }

    /**
     * Emitted when the terminal's icon_title field is modified.
     * @deprecated Superceeded by java-gnome 4.0; this method may or may not
     *             exist in the new bindings but if it does, it will likely have 
     *             a different name or signature in order that the presented API
     *             is a more algorithmic mapping of the underlying native libraries.
     */
    private void handleIconTitleChanged() {
        fireTerminalEvent(new TerminalEvent(this, TerminalEvent.Type.ICON_TITLE_CHANGED));
    }

    /**
     * Emitted at the child application's request.
     * @deprecated Superceeded by java-gnome 4.0; this method may or may not
     *             exist in the new bindings but if it does, it will likely have 
     *             a different name or signature in order that the presented API
     *             is a more algorithmic mapping of the underlying native libraries.
     */
    private void handleIconifyWindow() {
        fireTerminalEvent(new TerminalEvent(this, TerminalEvent.Type.ICONIFY_WINDOW));
    }

    /**
     * Emitted when the user hits the '+' key while holding the Control key.
     * @deprecated Superceeded by java-gnome 4.0; this method may or may not
     *             exist in the new bindings but if it does, it will likely have 
     *             a different name or signature in order that the presented API
     *             is a more algorithmic mapping of the underlying native libraries.
     */
    private void handleIncreaseFontSize() {
        fireTerminalEvent(new TerminalEvent(this, TerminalEvent.Type.INCREASE_FONT_SIZE));
    }

    /**
     * Emitted at the child application's request.
     * @deprecated Superceeded by java-gnome 4.0; this method may or may not
     *             exist in the new bindings but if it does, it will likely have 
     *             a different name or signature in order that the presented API
     *             is a more algorithmic mapping of the underlying native libraries.
     */
    private void handleLowerWindow() {
        fireTerminalEvent(new TerminalEvent(this, TerminalEvent.Type.LOWER_WINDOW));
    }

    /**
     * Emitted at the child application's request.
     * @deprecated Superceeded by java-gnome 4.0; this method may or may not
     *             exist in the new bindings but if it does, it will likely have 
     *             a different name or signature in order that the presented API
     *             is a more algorithmic mapping of the underlying native libraries.
     */
    private void handleMaximizeWindow() {
        fireTerminalEvent(new TerminalEvent(this, TerminalEvent.Type.MAXIMIZE_WINDOW));
    }

    /**
     * Emitted at the child application's request.
     * @param x the terminal's desired location, X coordinate.
     * @param y the terminal's desired location, Y coordinate.
     * @deprecated Superceeded by java-gnome 4.0; this method may or may not
     *             exist in the new bindings but if it does, it will likely have 
     *             a different name or signature in order that the presented API
     *             is a more algorithmic mapping of the underlying native libraries.
     */
    private void handleMoveWindow(int x, int y) {
        MoveWindowEvent event = new MoveWindowEvent(this,
                MoveWindowEvent.Type.MOVE_WINDOW, x, y);
        
        fireTerminalMoveWindowEvent(event);
    }

    /**
     * Emitted at the child application's request.
     * @deprecated Superceeded by java-gnome 4.0; this method may or may not
     *             exist in the new bindings but if it does, it will likely have 
     *             a different name or signature in order that the presented API
     *             is a more algorithmic mapping of the underlying native libraries.
     */
    private void handleRaiseWindow() {
        fireTerminalEvent(new TerminalEvent(this, TerminalEvent.Type.RAISE_WINDOW));
    }

    /**
     * Emitted at the child application's request.
     * @deprecated Superceeded by java-gnome 4.0; this method may or may not
     *             exist in the new bindings but if it does, it will likely have 
     *             a different name or signature in order that the presented API
     *             is a more algorithmic mapping of the underlying native libraries.
     */
    private void handleRefreshWindow() {
        fireTerminalEvent(new TerminalEvent(this, TerminalEvent.Type.REFRESH_WINDOW));
    }

    /**
     * Emitted at the child application's request.
     * @param width the desired width in pixels.
     * @param height the desired height in pixels.
     * @deprecated Superceeded by java-gnome 4.0; this method may or may not
     *             exist in the new bindings but if it does, it will likely have 
     *             a different name or signature in order that the presented API
     *             is a more algorithmic mapping of the underlying native libraries.
     */
    private void handleResizeWindow(int width, int height) {
        ResizeWindowEvent event = new ResizeWindowEvent(this,
                ResizeWindowEvent.Type.RESIZE_WINDOW, width, height);
        
        fireTerminalResizeWindowEvent(event);
    }

    /**
     * Emitted at the child application's request.
     * @deprecated Superceeded by java-gnome 4.0; this method may or may not
     *             exist in the new bindings but if it does, it will likely have 
     *             a different name or signature in order that the presented API
     *             is a more algorithmic mapping of the underlying native libraries.
     */
    private void handleRestoreWindow() {
        fireTerminalEvent(new TerminalEvent(this, TerminalEvent.Type.RESTORE_WINDOW));
    }

    /**
     * Emitted whenever the contents of the status line are modified or cleared.
     * @deprecated Superceeded by java-gnome 4.0; this method may or may not
     *             exist in the new bindings but if it does, it will likely have 
     *             a different name or signature in order that the presented API
     *             is a more algorithmic mapping of the underlying native libraries.
     */
    private void handleStatusLineChanged() {
        fireTerminalEvent(new TerminalEvent(this, TerminalEvent.Type.STATUS_LINE_CHANGED));
    }

    /**
     * An internal signal used for communication between the terminal and its accessibility peer. 
     * May not be emitted under certain circumstances.
     * @deprecated Superceeded by java-gnome 4.0; this method may or may not
     *             exist in the new bindings but if it does, it will likely have 
     *             a different name or signature in order that the presented API
     *             is a more algorithmic mapping of the underlying native libraries.
     */
    private void handleTextDeleted() {
        fireTerminalEvent(new TerminalEvent(this, TerminalEvent.Type.TEXT_DELETED));
    }

    /**
     * An internal signal used for communication between the terminal and its accessibility peer. 
     * May not be emitted under certain circumstances.
     * @deprecated Superceeded by java-gnome 4.0; this method may or may not
     *             exist in the new bindings but if it does, it will likely have 
     *             a different name or signature in order that the presented API
     *             is a more algorithmic mapping of the underlying native libraries.
     */
    private void handleTextInserted() {
        fireTerminalEvent(new TerminalEvent(this, TerminalEvent.Type.TEXT_INSERTED));
    }

    /**
     * An internal signal used for communication between the terminal and its accessibility peer. 
     * May not be emitted under certain circumstances.
     * @deprecated Superceeded by java-gnome 4.0; this method may or may not
     *             exist in the new bindings but if it does, it will likely have 
     *             a different name or signature in order that the presented API
     *             is a more algorithmic mapping of the underlying native libraries.
     */
    private void handleTextModified() {
        fireTerminalEvent(new TerminalEvent(this, TerminalEvent.Type.TEXT_MODIFIED));
    }

    /**
     * An internal signal used for communication between the terminal and its accessibility peer. 
     * May not be emitted under certain circumstances.
     * @param delta <i>Unknown this field was copied at it is from the <code>C</code> source code 
     * of the vte widget since it's missing from the documentation.</i>
     * @deprecated Superceeded by java-gnome 4.0; this method may or may not
     *             exist in the new bindings but if it does, it will likely have 
     *             a different name or signature in order that the presented API
     *             is a more algorithmic mapping of the underlying native libraries.
     */
    private void handleTextScrolled(int delta) {
        TextScrolledEvent event = new TextScrolledEvent(this,
                TextScrolledEvent.Type.TEXT_SCROLLED, delta);
            
        fireTerminalTextScrolledEvent(event);
    }

    /**
     * Emitted when the terminal's window_title field is modified.
     * @deprecated Superceeded by java-gnome 4.0; this method may or may not
     *             exist in the new bindings but if it does, it will likely have 
     *             a different name or signature in order that the presented API
     *             is a more algorithmic mapping of the underlying native libraries.
     */
    private void handleWindowTitleChanged() {
        fireTerminalEvent(new TerminalEvent(this, TerminalEvent.Type.WINDOW_TITLE_CHANGED));
    }
	
	/**
	* Implementation method to build an EventMap for this widget class.
	* Not useful (or supported) for application use.
	*/
	private static void addEvents(EventMap anEvtMap) {
		anEvtMap.addEvent("child-exited", "handleChildExited", TerminalEvent.Type.CHILD_EXITED, TerminalListener.class);
		anEvtMap.addEvent("selection-changed", "handleSelectionChanged", TerminalEvent.Type.SELECTION_CHANGED, TerminalListener.class);
		anEvtMap.addEvent("contents-changed", "handleContentsChanged", TerminalEvent.Type.CONTENTS_CHANGED, TerminalListener.class);
		anEvtMap.addEvent("cursor-moved", "handleCursorMoved", TerminalEvent.Type.CURSOR_MOVED, TerminalListener.class);
		anEvtMap.addEvent("decrease-font-size", "handleDecreaseFontSize", TerminalEvent.Type.DECREASE_FONT_SIZE, TerminalListener.class);
		anEvtMap.addEvent("deiconify-window", "handleDeiconifyWindow", TerminalEvent.Type.DEICONIFY_WINDOW, TerminalListener.class);
		anEvtMap.addEvent("emulation-changed", "handleEmulationChanged", TerminalEvent.Type.EMULATION_CHANGED, TerminalListener.class);
		anEvtMap.addEvent("encoding-changed", "handleEncodingChanged", TerminalEvent.Type.ENCODING_CHANGED, TerminalListener.class);
		anEvtMap.addEvent("eof", "handleEof", TerminalEvent.Type.EOF, TerminalListener.class);
		anEvtMap.addEvent("icon-title-changed", "handleIconTitleChanged", TerminalEvent.Type.ICON_TITLE_CHANGED, TerminalListener.class);
		anEvtMap.addEvent("iconify-window", "handleIconifyWindow", TerminalEvent.Type.ICONIFY_WINDOW, TerminalListener.class);
		anEvtMap.addEvent("increase-font-size", "handleIncreaseFontSize", TerminalEvent.Type.INCREASE_FONT_SIZE, TerminalListener.class);
		anEvtMap.addEvent("lower-window", "handleLowerWindow", TerminalEvent.Type.LOWER_WINDOW, TerminalListener.class);
		anEvtMap.addEvent("maximize-window", "handleMaximizeWindow", TerminalEvent.Type.MAXIMIZE_WINDOW, TerminalListener.class);
		anEvtMap.addEvent("raise-window", "handleRaiseWindow", TerminalEvent.Type.RAISE_WINDOW, TerminalListener.class);
		anEvtMap.addEvent("refresh-window", "handleRefreshWindow", TerminalEvent.Type.REFRESH_WINDOW, TerminalListener.class);
		anEvtMap.addEvent("restore-window", "handleRestoreWindow", TerminalEvent.Type.RESTORE_WINDOW, TerminalListener.class);
		anEvtMap.addEvent("status-line-changed", "handleStatusLineChanged", TerminalEvent.Type.STATUS_LINE_CHANGED, TerminalListener.class);
		anEvtMap.addEvent("text-deleted", "handleTextDeleted", TerminalEvent.Type.TEXT_DELETED, TerminalListener.class);
		anEvtMap.addEvent("text-inserted", "handleTextInserted", TerminalEvent.Type.TEXT_INSERTED, TerminalListener.class);
		anEvtMap.addEvent("window-title-changed", "handleWindowTitleChanged", TerminalEvent.Type.WINDOW_TITLE_CHANGED, TerminalListener.class);
		anEvtMap.addEvent("text-modified", "handleTextModified", TerminalEvent.Type.TEXT_MODIFIED, TerminalListener.class);
		
		anEvtMap.addEvent("commit", "handleCommit", CommitEvent.Type.COMMIT, TerminalListener.class);
		anEvtMap.addEvent("char-size-changed", "handleCharSizeChanged", CharacterSizeChangedEvent.Type.CHAR_SIZE_CHANGED, TerminalListener.class);
		anEvtMap.addEvent("move-window", "handleMoveWindow", MoveWindowEvent.Type.MOVE_WINDOW, TerminalListener.class);
		anEvtMap.addEvent("resize-window", "handleResizeWindow", ResizeWindowEvent.Type.RESIZE_WINDOW, TerminalListener.class);
		anEvtMap.addEvent("text-scrolled", "handleTextScrolled", TextScrolledEvent.Type.TEXT_SCROLLED, TerminalListener.class);
	}
	
    /*-------- NATIVE METHODS -----------*/
    static native Handle vte_terminal_new();
    static native int vte_terminal_fork_command(Handle handle,
                String command, String[] argv,
                String directory,
                boolean lastlog,
                boolean utmp,
                boolean wtmp);

    /* Associate a pty file descriptor with a terminal */
    static native void vte_terminal_set_pty(Handle handle, int pty_master);

    /* Send data to the terminal to display, or to the terminal's forked command
    to handle in some way.  If it's 'cat', they should be the same. */
    static native void vte_terminal_feed(Handle handle, String data, int length);
    static native void vte_terminal_feed_child (Handle handle,
                 String data, long length);

    /* Copy currently-selected text to the clipboard, or from the clipboard to
    * the terminal. */
    static native void vte_terminal_copy_clipboard (Handle handle);
    static native void vte_terminal_paste_clipboard (Handle handle);
    static native void vte_terminal_copy_primary(Handle handle);
    static native void vte_terminal_paste_primary(Handle handle);

    /* Set the terminal's size. */
    static native void vte_terminal_set_size(Handle handle, int columns, int rows);

    /* Set various one-off settings. */
    static native void vte_terminal_set_audible_bell(Handle handle, boolean is_audible);
    static native boolean vte_terminal_get_audible_bell(Handle handle);
    static native void vte_terminal_set_visible_bell(Handle handle, boolean is_visible);
    static native boolean vte_terminal_get_visible_bell(Handle handle);
    static native void vte_terminal_set_scroll_on_output(Handle handle, boolean scroll);
    static native void vte_terminal_set_scroll_on_keystroke(Handle handle, boolean scroll);

    /* Set the color scheme. */
    static native void vte_terminal_set_color_dim(Handle handle, Handle gdkColor);
    static native void vte_terminal_set_color_bold(Handle handle, Handle gdkColor);
    static native void vte_terminal_set_color_foreground(Handle handle, Handle gdkColor);
    static native void vte_terminal_set_color_background(Handle handle, Handle gdkColor);
    
    // foreground, background and palette are gdk.Color
    static native void vte_terminal_set_colors(Handle handle, Handle foreground, Handle background, Handle palette, int palette_size);
    static native void vte_terminal_set_default_colors(Handle handle);

    /* Background effects. */

    //image is a GdkPixbuf
    static native void vte_terminal_set_background_image(Handle handle, Handle image);
    static native void vte_terminal_set_background_image_file(Handle handle, String path);
    static native void vte_terminal_set_background_saturation(Handle handle, double saturation);
    static native void vte_terminal_set_background_transparent(Handle handle, boolean transparent);

    /* Set whether or not the cursor blinks. */
    static native void vte_terminal_set_cursor_blinks(Handle handle, boolean blink);

    /* Set the number of scrollback lines, above or at an internal minimum. */
    static native void vte_terminal_set_scrollback_lines(Handle handle, int lines);

    /* Append the input method menu items to a given shell. */
    // menushell: GtkMenuShell
//  static native void vte_terminal_im_append_menuitems(Handle handle, Handle menushell);

    /* Set or retrieve the current font. */
    // font_desc: PangoFontDescription
    static native void vte_terminal_set_font(Handle handle, Handle font_desc);
//  static native void vte_terminal_set_font_from_string(Handle handle, byte[] name);
    
    //return: PangoFontDescription.
    static native Handle vte_terminal_get_font(Handle handle);
//  static native boolean vte_terminal_get_using_xft(Handle handle);
    static native void vte_terminal_set_allow_bold(Handle handle, boolean allow_bold);
    static native boolean vte_terminal_get_allow_bold(Handle handle);

    /** 
     * Checks if the terminal currently contains selected text. 
     * Note that this is different from determining if the terminal 
     * is the owner of any GtkClipboard items. 
     * @deprecated Superceeded by java-gnome 4.0; this method may or may not
     *             exist in the new bindings but if it does, it will likely have 
     *             a different name or signature in order that the presented API
     *             is a more algorithmic mapping of the underlying native libraries.
     */
    static native boolean vte_terminal_get_has_selection(Handle handle);

    /* Set the list of word chars, optionally using hyphens to specify ranges
    * (to get a hyphen, place it first), and check if a character is in the
    * range. */
//  static native void vte_terminal_set_word_chars(Handle handle, byte[] spec);
//  static native boolean vte_terminal_is_word_char(Handle handle, char c);

    /* Set what happens when the user strikes backspace or delete. */
    // binding VteTerminalEraseBinding
//  static native void vte_terminal_set_backspace_binding(Handle handle, int binding);
    // binding VteTerminalEraseBinding
//  static native void vte_terminal_set_delete_binding(Handle handle, int binding);

    /* Manipulate the autohide setting. */
//  static native void vte_terminal_set_mouse_autohide(Handle handle, boolean setting);
//  static native boolean vte_terminal_get_mouse_autohide(Handle handle);

    /* Reset the terminal, optionally clearing the tab stops and line history. */
    static native void vte_terminal_reset(Handle handle, boolean full, boolean clear_history);

    /* Read the contents of the terminal, using a callback function to determine
    * if a particular location on the screen (0-based) is interesting enough to
    * include.  Each byte in the returned string will have a corresponding
    * struct vte_char_attributes in the passed GArray, if the array was not NULL.
    * Note that it will have one entry per byte, not per character, so indexes
    * should match up exactly. */
    /*static byte[] vte_terminal_get_text(Handle handle,
                boolean(*is_selected)(Handle handle,
                           int column,
                           int row,
                           gpointer data),
                gpointer data,
                GArray *attributes);
    static byte[] vte_terminal_get_text_range(Handle handle,
                  int start_row, int start_col,
                  int end_row, int end_col,
                  boolean(*is_selected)(Handle handle,
                             int column,
                             int row,
                             gpointer data),
                  gpointer data,
                  GArray *attributes);*/
//  static native void vte_terminal_get_cursor_position(Handle handle, int[] column, int[] row);

    /* Display string matching:  clear all matching expressions. */
//  static native void vte_terminal_match_clear_all(Handle handle);

    /* Add a matching expression, returning the tag the widget assigns to that
     * expression. */
//  static native int vte_terminal_match_add(Handle handle, byte[] match);
    /* Remove a matching expression by tag. */
//  static native void vte_terminal_match_remove(Handle handle, int tag);

    /* Check if a given cell on the screen contains part of a matched string.  If
    * it does, return the string, and store the match tag in the optional tag
    * argument. */
//  static native String vte_terminal_match_check(Handle handle, int column, int row, int[] tag);

    /* Set the emulation type.  Most of the time you won't need this. */
    static native void vte_terminal_set_emulation(Handle handle, String emulation);
    static native String vte_terminal_get_emulation(Handle handle);

    /* Set the character encoding.  Most of the time you won't need this. */
    static native void vte_terminal_set_encoding(Handle handle, String codeset);
    static native String vte_terminal_get_encoding(Handle handle);

    /* Get the contents of the status line. */
    static native String vte_terminal_get_status_line(Handle handle);

    /* Get the padding the widget is using. */
//  static native void vte_terminal_get_padding(Handle handle, int[] xpad, int[] ypad);
    
	/* Accessors for bindings. */
	//return: GtkAdjustment
	static native Handle vte_terminal_get_adjustment(Handle handle);
//	static native long vte_terminal_get_char_width(Handle handle);
//	static native long vte_terminal_get_char_height(Handle handle);
//	static native long vte_terminal_get_char_descent(Handle handle);
//	static native long vte_terminal_get_char_ascent(Handle handle);
//	static native long vte_terminal_get_row_count(Handle handle);
//	static native long vte_terminal_get_column_count(Handle handle);
//	static native String vte_terminal_get_window_title(Handle handle);
//	static native String vte_terminal_get_icon_title(Handle handle);
}
