/*
 * Java-Gnome Bindings Library
 *
 * Copyright 1998-2004 the Java-Gnome Team, all rights reserved.
 *
 * The Java-Gnome Team Members:
 *   Jean Van Wyk <jeanvanwyk@iname.com>
 *   Jeffrey S. Morgan <jeffrey.morgan@bristolwest.com>
 *   Dan Bornstein <danfuzz@milk.com>
 *
 * The Java-Gnome bindings library is free software distributed under
 * the terms of the GNU Library General Public License version 2.
 */
package org.gnu.gnomevte.event;

import org.gnu.glib.EventType;
import org.gnu.gtk.event.GtkEvent;

/**
 * An event representing a move window action by a {@link org.gnu.gnomevte.Terminal} widget.
 * This event is fired when the child application (the command forked) requests it.
 *
 * @deprecated This class is part of the java-gnome 2.x family of libraries,
 *             which, due to their inefficiency and complexity, are no longer
 *             being maintained and have been abandoned by the java-gnome
 *             project. Signal handling has been completely re-designed 
 *             in java-gnome 4.0, so there will be no direct correspondant
 *             for this class. See individual inner interfaces in classes
 *             within <code>org.gnome.vte</code>
 */
public class MoveWindowEvent extends GtkEvent {
	
	/**
	 * The terminal's desired location, X coordinate.
	 */
	private final int xCoordinate;
	
	/**
	 * The terminal's desired location, Y coordinate.
	 */
	private final int yCoordinate;
	
	/**
	 * Type of a TerminalMoveWindowEvent.
	 */
	public static class Type extends EventType {
		
		/**
		 * The constructor.
		 * @param id a unique id.
		 * @param name the name of the event.
		 */
		private Type(int id, String name) {
			super(id, name);
		}
		
		/**
		 * Emitted at the child application's request.
		 */
		public static final Type MOVE_WINDOW = new Type(1, "MOVE_WINDOW");
	}

	/**
	 * Constructor for TerminalMoveWindowEvent.
	 * @param source the source of the event.
	 * @param type the event type.
     * @param xCoordinate the terminal's desired location, X coordinate.
     * @param yCoordinate the terminal's desired location, Y coordinate.
	 */
	public MoveWindowEvent(Object source, EventType type, int xCoordinate,
            int yCoordinate) {
		super(source, type);
        this.xCoordinate = xCoordinate;
        this.yCoordinate = yCoordinate;
	}

	/**
	 * This method compares the type of the current event to 
	 * the one provided as an argument. 
	 * @param aType the type to compare to.
	 * @return <code>true</code> if the events are of same type.
	 */
	public boolean isOfType(MoveWindowEvent.Type aType) {
		return (this.type.getID() == aType.getID());
	}
	
	/**
	 * Gets the X coordinate.
	 * @return the new X coordinate.
	 */
	public int getXCoordinate() {
		return this.xCoordinate;
	}
	
	/**
	 * Gets the Y coordinate.
	 * @return the new Y coordinate.
	 */
	public int getYCoordinate() {
		return this.yCoordinate;
	}
}


