/*
 * Java-Gnome Bindings Library
 *
 * Copyright 1998-2004 the Java-Gnome Team, all rights reserved.
 *
 * The Java-Gnome Team Members:
 *   Jean Van Wyk <jeanvanwyk@iname.com>
 *   Jeffrey S. Morgan <jeffrey.morgan@bristolwest.com>
 *   Dan Bornstein <danfuzz@milk.com>
 *
 * The Java-Gnome bindings library is free software distributed under
 * the terms of the GNU Library General Public License version 2.
 */
package org.gnu.gnomevte.event;

import org.gnu.glib.EventType;
import org.gnu.gtk.event.GtkEvent;

/**
 * An event representing a text scrolled action by a {@link org.gnu.gnomevte.Terminal} widget.
 * An internal event used for communication between the terminal and its accessibility peer. 
 * May not be emitted under certain circumstances.
 *
 * @deprecated This class is part of the java-gnome 2.x family of libraries,
 *             which, due to their inefficiency and complexity, are no longer
 *             being maintained and have been abandoned by the java-gnome
 *             project. Signal handling has been completely re-designed 
 *             in java-gnome 4.0, so there will be no direct correspondant
 *             for this class. See individual inner interfaces in classes
 *             within <code>org.gnome.vte</code>
 */
public class TextScrolledEvent extends GtkEvent {
	
	/**
	 * The scroll delta.
	 */
	private final int scrollDelta;
	
	/**
	 * Type of a TerminalEvent.
	 */
	public static class Type extends EventType {
		
		/**
		 * The constructor.
		 * @param id a unique id.
		 * @param name the name of the event.
		 */
		private Type(int id, String name) {
			super(id, name);
		}
		
		/**
		 * An internal signal used for communication between the terminal and its 
		 * accessibility peer. 
		 * May not be emitted under certain circumstances.
		 */
		public static final Type TEXT_SCROLLED = new Type(1, "TEXT_SCROLLED");
	}

	/**
	 * Constructor for TerminalTextScrolledEvent.
	 * @param source the source of the event.
	 * @param type the event type.
     * @param scrollDelta the scroll delta.
	 */
	public TextScrolledEvent(Object source, EventType type, int scrollDelta) {
		super(source, type);
        this.scrollDelta = scrollDelta;
	}

	/**
	 * This method compares the type of the current event to 
	 * the one provided as an argument. 
	 * @param aType the type to compare to.
	 * @return <code>true</code> if the events are of same type.
	 */
	public boolean isOfType(TextScrolledEvent.Type aType) {
		return (this.type.getID() == aType.getID());
	}
	
	/**
	 * Gets the scroll delta.
	 * @return the scroll delta.
	 */
	public int getScrollDelta() {
		return this.scrollDelta;
	}
}


