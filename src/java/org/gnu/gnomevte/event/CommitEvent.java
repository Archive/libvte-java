/*
 * Java-Gnome Bindings Library
 *
 * Copyright 1998-2004 the Java-Gnome Team, all rights reserved.
 *
 * The Java-Gnome Team Members:
 *   Jean Van Wyk <jeanvanwyk@iname.com>
 *   Jeffrey S. Morgan <jeffrey.morgan@bristolwest.com>
 *   Dan Bornstein <danfuzz@milk.com>
 *
 * The Java-Gnome bindings library is free software distributed under
 * the terms of the GNU Library General Public License version 2.
 */
package org.gnu.gnomevte.event;

import org.gnu.glib.EventType;
import org.gnu.gtk.event.GtkEvent;

/**
 * An event representing a commit action by a {@link org.gnu.gnomevte.Terminal} widget.
 * This event is fired every time that data is sent to the terminal.
 * This normally happens after pasting some text or by interacting with the keyboard.
 *
 * @deprecated This class is part of the java-gnome 2.x family of libraries,
 *             which, due to their inefficiency and complexity, are no longer
 *             being maintained and have been abandoned by the java-gnome
 *             project. Signal handling has been completely re-designed 
 *             in java-gnome 4.0, so there will be no direct correspondant
 *             for this class. See individual inner interfaces in classes
 *             within <code>org.gnome.vte</code>
 */
public class CommitEvent extends GtkEvent {
	
	/**
	 * The text that is committed (sent) to the Terminal. 
	 */
	private final String committedText;

	/**
	 * Type of a TerminalCommitEvent.
	 */
	public static class Type extends EventType {
		
		/**
		 * The constructor.
		 * @param id a unique id.
		 * @param name the name of the event.
		 */
		private Type(int id, String name) {
			super(id, name);
		}
		
		/**
		 * Emitted whenever the terminal receives input from the user and prepares 
		 * to send it to the child process. The event is emitted even when there 
		 * is no child process. This event is fired every time a key is pressed or 
		 * text is pasted into the Terminal.
		 */
		public static final Type COMMIT = new Type(1, "COMMIT");
	}

	/**
	 * Constructor for TerminalCommitEvent.
	 * @param source the source of the event.
	 * @param type the event type.
     * @param committedText the text that is committed (sent) to the Terminal.
	 */
	public CommitEvent(Object source, EventType type,
            String committedText) {
		super(source, type);
        this.committedText = committedText;
	}

	/**
	 * This method compares the type of the current event to 
	 * the one provided as an argument. 
	 * @param aType the type to compare to.
	 * @return <code>true</code> if the events are of same type.
	 */
	public boolean isOfType(CommitEvent.Type aType) {
		return (this.type.getID() == aType.getID());
	}
	
	/**
	 * Get's the text that is being committed to the Terminal.
	 * @return the committed text.
	 */
	public String getCommittedText() {
		return this.committedText;
	}
}


