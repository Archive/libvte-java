/*
 * Java-Gnome Bindings Library
 *
 * Copyright 1998-2004 the Java-Gnome Team, all rights reserved.
 *
 * The Java-Gnome Team Members:
 *   Jean Van Wyk <jeanvanwyk@iname.com>
 *   Jeffrey S. Morgan <jeffrey.morgan@bristolwest.com>
 *   Dan Bornstein <danfuzz@milk.com>
 *
 * The Java-Gnome bindings library is free software distributed under
 * the terms of the GNU Library General Public License version 2.
 */
package org.gnu.gnomevte.event;

import org.gnu.glib.EventType;
import org.gnu.gtk.event.GtkEvent;

/**
 * An event representing an action by a {@link org.gnu.gnomevte.Terminal} widget.
 * This event is fired every time that the character size changed.
 * This normally happens after changing the terminal's font. 
 *
 * @deprecated This class is part of the java-gnome 2.x family of libraries,
 *             which, due to their inefficiency and complexity, are no longer
 *             being maintained and have been abandoned by the java-gnome
 *             project. Signal handling has been completely re-designed 
 *             in java-gnome 4.0, so there will be no direct correspondant
 *             for this class. See individual inner interfaces in classes
 *             within <code>org.gnome.vte</code>
 */
public class CharacterSizeChangedEvent extends GtkEvent {
	
	/**
	 * The new character cell width.
	 */
	private final int characterWidth;
	
	/**
	 * The new character cell height.
	 */
	private final int characterHeight;
	
	/**
	 * Type of a TerminalCharacterSizeChangedEvent.
	 */
	public static class Type extends EventType {
		
		/**
		 * The constructor.
		 * @param id a unique id.
		 * @param name the name of the event.
		 */
		private Type(int id, String name) {
			super(id, name);
		}
		
		/**
		 * Emitted whenever the selection of a new font causes the values of the 
		 * character width or character height fields to change.
		 */
		public static final Type CHAR_SIZE_CHANGED = new Type(1, "CHAR_SIZE_CHANGED");
	}

	/**
	 * Constructor for TerminalCharacterSizeChangedEvent.
	 * @param source the source of the event.
	 * @param type the event type.
     * @param characterWidth the new character cell width.
     * @param characterHeight the new character cell height.
	 */
	public CharacterSizeChangedEvent(Object source, EventType type,
            int characterWidth, int characterHeight) {
		super(source, type);
        this.characterHeight = characterHeight;
        this.characterWidth = characterWidth;
	}

	/**
	 * This method compares the type of the current event to 
	 * the one provided as an argument. 
	 * @param aType the type to compare to.
	 * @return <code>true</code> if the events are of same type.
	 */
	public boolean isOfType(CharacterSizeChangedEvent.Type aType) {
		return (this.type.getID() == aType.getID());
	}
	
	/**
	 * Gets the new character cell width. 
	 * @return the new character cell width.
	 */
	public int getCharacterWidth() {
		return this.characterWidth;
	}
	
	/**
	 * Gets the new character cell height. 
	 * @return the new character cell height.
	 */
	public int getCharacterHeight() {
		return this.characterHeight;
	}
}


