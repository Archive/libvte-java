/*
 * Java-Gnome Bindings Library
 *
 * Copyright 1998-2004 the Java-Gnome Team, all rights reserved.
 *
 * The Java-Gnome Team Members:
 *   Jean Van Wyk <jeanvanwyk@iname.com>
 *   Jeffrey S. Morgan <jeffrey.morgan@bristolwest.com>
 *   Dan Bornstein <danfuzz@milk.com>
 *
 * The Java-Gnome bindings library is free software distributed under
 * the terms of the GNU Library General Public License version 2.
 */
package org.gnu.gnomevte.event;

import org.gnu.glib.EventType;
import org.gnu.gtk.event.GtkEvent;

/**
 * An event representing an action by a {@link org.gnu.gnomevte.Terminal} widget.
 * This event is fired for every generic event that requires no exra information.
 *
 * @deprecated This class is part of the java-gnome 2.x family of libraries,
 *             which, due to their inefficiency and complexity, are no longer
 *             being maintained and have been abandoned by the java-gnome
 *             project. Signal handling has been completely re-designed 
 *             in java-gnome 4.0, so there will be no direct correspondant
 *             for this class. See individual inner interfaces in classes
 *             within <code>org.gnome.vte</code>
 */
public class TerminalEvent extends GtkEvent {
	
	/**
	 * Type of a TerminalEvent.
	 */
	public static class Type extends EventType {
		
		/**
		 * The constructor.
		 * @param id a unique id.
		 * @param name the name of the event.
		 */
		private Type(int id, String name) {
			super(id, name);
		}
		
		/**
		 * Emitted when the terminal detects that a child started using
		 * {@link org.gnu.gnomevte.Terminal#forkCommand} has exited.
		 */
		public static final Type CHILD_EXITED = new Type(1, "CHILD_EXITED");
		
		/**
		 * Emitted whenever the contents of terminal's selection changes.
		 */
		public static final Type SELECTION_CHANGED = new Type(2, "SELECTION_CHANGED");
		
		/**
		 * Emitted whenever the visible appearance of the terminal has changed. 
		 */
		public static final Type CONTENTS_CHANGED = new Type(3, "CONTENTS_CHANGED");
		
		/**
		 * Emitted whenever the cursor moves to a new character cell.
		 */
		public static final Type CURSOR_MOVED = new Type(4, "CURSOR_MOVED");
		
		/**
		 * Emitted when the user hits the '-' key while holding the Control key.
		 */
		public static final Type DECREASE_FONT_SIZE = new Type(5, "DECREASE_FONT_SIZE");

		/**
		 * Emitted at the child application's request.
		 */
		public static final Type DEICONIFY_WINDOW = new Type(6, "DEICONIFY_WINDOW");

		/**
		 * Emitted when the terminal emulation changes.
		 */
		public static final Type EMULATION_CHANGED = new Type(7, "EMULATION_CHANGED");

		/**
		 * Emitted when the encoding of the terminal changes.
		 */
		public static final Type ENCODING_CHANGED = new Type(8, "ENCODING_CHANGED");

		/**
		 * Emitted when an End_Of_File is issued to the terminal.
		 */
		public static final Type EOF = new Type(9, "EOF");

		/**
		 * Emitted when the terminal's icon_title field is modified.
		 */
		public static final Type ICON_TITLE_CHANGED = new Type(10, "ICON_TITLE_CHANGED");

		/**
		 * Emitted at the child application's request.
		 */
		public static final Type ICONIFY_WINDOW = new Type(11, "ICONIFY_WINDOW");

		/**
		 * Emitted when the user hits the '+' key while holding the Control key.
		 */
		public static final Type INCREASE_FONT_SIZE = new Type(12, "INCREASE_FONT_SIZE");

		/**
		 * Emitted at the child application's request.
		 */
		public static final Type LOWER_WINDOW = new Type(13, "LOWER_WINDOW");

		/**
		 * Emitted at the child application's request.
		 */
		public static final Type MAXIMIZE_WINDOW = new Type(14, "MAXIMIZE_WINDOW");

		/**
		 * Emitted at the child application's request.
		 */
		public static final Type RAISE_WINDOW = new Type(15, "RAISE_WINDOW");

		/**
		 * Emitted at the child application's request.
		 */
		public static final Type REFRESH_WINDOW = new Type(16, "REFRESH_WINDOW");

		/**
		 * Emitted at the child application's request.
		 */
		public static final Type RESTORE_WINDOW = new Type(17, "RESTORE_WINDOW");

		/**
		 * Emitted whenever the contents of the status line are modified or cleared.
		 */
		public static final Type STATUS_LINE_CHANGED = new Type(18, "STATUS_LINE_CHANGED");

		/**
		 * An internal signal used for communication between the terminal and its accessibility peer. 
		 * May not be emitted under certain circumstances.
		 */
		public static final Type TEXT_DELETED = new Type(19, "TEXT_DELETED");

		/**
		 * An internal signal used for communication between the terminal and its accessibility peer. 
		 * May not be emitted under certain circumstances.
		 */
		public static final Type TEXT_INSERTED = new Type(20, "TEXT_INSERTED");

		/**
		 * An internal signal used for communication between the terminal and its accessibility peer. 
		 * May not be emitted under certain circumstances.
		 */
		public static final Type TEXT_MODIFIED = new Type(21, "TEXT_MODIFIED");

		/**
		 * Emitted when the terminal's window_title field is modified.
		 */
		public static final Type WINDOW_TITLE_CHANGED = new Type(22, "WINDOW_TITLE_CHANGED");
	}

	/**
	 * Constructor for TerminalEvent.
	 * @param source the source of the event.
	 * @param type the event type.
	 */
	public TerminalEvent(Object source, EventType type) {
		super(source, type);
	}

	/**
	 * This method compares the type of the current event to 
	 * the one provided as an argument. 
	 * @param aType the type to compare to.
	 * @return <code>true</code> if the events are of same type.
	 */
	public boolean isOfType(TerminalEvent.Type aType) {
		return (this.type.getID() == aType.getID());
	}
}


