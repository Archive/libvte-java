/*
 * Java-Gnome Bindings Library
 *
 * Copyright 1998-2004 the Java-Gnome Team, all rights reserved.
 *
 * The Java-Gnome Team Members:
 *   Jean Van Wyk <jeanvanwyk@iname.com>
 *   Jeffrey S. Morgan <jeffrey.morgan@bristolwest.com>
 *   Dan Bornstein <danfuzz@milk.com>
 *
 * The Java-Gnome bindings library is free software distributed under
 * the terms of the GNU Library General Public License version 2.
 */
package org.gnu.gnomevte.event;

import org.gnu.glib.EventType;
import org.gnu.gtk.event.GtkEvent;

/**
 * An event representing an action by a {@link org.gnu.gnomevte.Terminal} widget.
 * This event is fired when the child application (the command forked) requests it.
 *
 * @deprecated This class is part of the java-gnome 2.x family of libraries,
 *             which, due to their inefficiency and complexity, are no longer
 *             being maintained and have been abandoned by the java-gnome
 *             project. Signal handling has been completely re-designed 
 *             in java-gnome 4.0, so there will be no direct correspondant
 *             for this class. See individual inner interfaces in classes
 *             within <code>org.gnome.vte</code>
 */
public class ResizeWindowEvent extends GtkEvent {
	
	/**
	 * The terminal's desired new window width.
	 */
	private final int windowWidth;
	
	/**
	 * The terminal's desired new window height.
	 */
	private final int windowHeight;
	
	/**
	 * Type of a TerminalResizeWindowEvent.
	 */
	public static class Type extends EventType {
		
		/**
		 * The constructor.
		 * @param id a unique id.
		 * @param name the name of the event.
		 */
		private Type(int id, String name) {
			super(id, name);
		}
		
		/**
		 * Emitted at the child application's request.
		 */
		public static final Type RESIZE_WINDOW = new Type(1, "RESIZE_WINDOW");
	}

	/**
	 * Constructor for TerminalResizeWindowEvent.
	 * @param source the source of the event.
	 * @param type the event type.
     * @param windowWidth
     * @param windowHeight
	 */
	public ResizeWindowEvent(Object source, EventType type, int windowWidth,
            int windowHeight) {
		super(source, type);
        this.windowWidth = windowWidth;
        this.windowHeight = windowHeight;
	}

	/**
	 * This method compares the type of the current event to 
	 * the one provided as an argument. 
	 * @param aType the type to compare to.
	 * @return <code>true</code> if the events are of same type.
	 */
	public boolean isOfType(ResizeWindowEvent.Type aType) {
		return (this.type.getID() == aType.getID());
	}
	
	/**
	 * Gets the new desired window width. 
	 * @return the new desired window width. 
	 */
	public int getWindowWidth() {
		return this.windowWidth;
	}
	
	/**
	 * Gets the new desired window height.
	 * @return the new desired window height.
	 */
	public int getWindowHeight() {
		return this.windowHeight;
	}
}


